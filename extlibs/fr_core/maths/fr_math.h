#pragma once

#include "helpers.h"
#include "mat3.h"
#include "mat4.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "quaternion.h"
#include "transforms.h"

//typedefs
typedef Vec4<float> vec4f;
typedef Vec3<float> vec3f;
typedef Vec2<float> vec2f;

typedef Vec4<int> vec4i;
typedef Vec3<int> vec3i;
typedef Vec2<int> vec2i;

typedef Mat4<float> mat4f;
typedef Mat3<float> mat3f;
typedef Mat4<int> mat4i;
typedef Mat3<int> mat3i;

typedef Quaternion<float> fquat;

#pragma once

#include <algorithm>
#include <stddef.h>
#include <stdint.h>

//constants
namespace MathConstants
{
	static constexpr double OneOverLogE = 0.36787944117144232159;
	static constexpr double logE = 2.71828182845904523536;
	static constexpr double log2E = 1.44269504088896340736;
	static constexpr double log10E = 0.434294481903251827651;
	static constexpr double ln2 = 0.693147180559945309417;
	static constexpr double ln10 = 2.30258509299404568402;
	static constexpr double Pi = 3.14159265358979323846;
	static constexpr double TwoPi = 6.28318530717958647692;
	static constexpr double FourPi = 12.56637061435917295385;
	static constexpr double HalfPi = 1.57079632679489661923;
	static constexpr double QuarterPi = 0.785398163397448309616;
	static constexpr double OneOverPi = 0.318309886183790671538;
	static constexpr double TwoOverPi = 0.636619772367581343076;
	static constexpr double TwoOverSqrtPi = 1.12837916709551257390;
	static constexpr double Sqrt2 = 1.41421356237309504880;
	static constexpr double OneOverSqrt2 = 0.707106781186547524401;
}

template <typename T>
constexpr T clampBetween(const T& val, const T& lowerBound, const T& upperBound)
{
	return std::max(lowerBound, std::min(val, upperBound));
}

template <typename T>
constexpr T lerp(const T& val1, const T& val2, T a)
{
	return ((1 - a) * val1) + (a * val2);
}

template <typename T>
constexpr T invLerp(const T& val1, const T& val2, const T& val_inbetween)
{
	return (val_inbetween - val1) / (val2 - val1);
}

template <typename T>
constexpr T toDegrees(const T& val)
{
	return static_cast<T>(val * 57.295779513082320876798154814105);
}

template <typename T>
constexpr T toRadians(const T& val)
{
	return static_cast<T>(val * 0.017453292519943295769236907684886);
}

constexpr bool isPowerOfTwo(const size_t val)
{
	return (val == 0) ? false : (val & (val - 1)) == 0u;
}

uint32_t nextPowerOfTwo(uint32_t val);
uint64_t nextPowerOfTwo(uint64_t val);

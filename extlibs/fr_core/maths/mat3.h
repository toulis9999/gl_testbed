#pragma once

#include <assert.h>
#include <cstdlib>

#include "vec3.h"

template <typename T>
struct Mat3
{
public:
	Mat3() = default;
	Mat3(T _m00, T _m01, T _m02, T _m10, T _m11, T _m12, T _m20, T _m21, T _m22)
	{
		m[0][0] = _m00; m[0][1] = _m01; m[0][2] = _m02;
		m[1][0] = _m10; m[1][1] = _m11; m[1][2] = _m12;
		m[2][0] = _m20; m[2][1] = _m21; m[2][2] = _m22;
	}

	Mat3(Vec3<T> v0, Vec3<T> v1, Vec3<T> v2)
	{
		m[0][0] = v0.x; m[0][1] = v0.y; m[0][2] = v0.z;
		m[1][0] = v1.x; m[1][1] = v1.y; m[1][2] = v1.z;
		m[2][0] = v2.x; m[2][1] = v2.y; m[2][2] = v2.z;
	}

	Mat3<T> operator+ (const Mat3<T>& rhs) const
	{
		return Mat3<T>(m[0][0] + rhs.m[0][0], m[0][1] + rhs.m[0][1], m[0][2] + rhs.m[0][2]
				, m[1][0] + rhs.m[1][0], m[1][1] + rhs.m[1][1], m[1][2] + rhs.m[1][2]
				, m[2][0] + rhs.m[2][0], m[2][1] + rhs.m[2][1], m[2][2] + rhs.m[2][2]);
	}

	Mat3<T> operator- (const Mat3<T>& rhs) const
	{
		return Mat3<T>(m[0][0] - rhs.m[0][0], m[0][1] - rhs.m[0][1], m[0][2] - rhs.m[0][2]
				, m[1][0] - rhs.m[1][0], m[1][1] - rhs.m[1][1], m[1][2] - rhs.m[1][2]
				, m[2][0] - rhs.m[2][0], m[2][1] - rhs.m[2][1], m[2][2] - rhs.m[2][2]);
	}

	Mat3<T> operator* (const Mat3<T>& rhs) const
	{
		return Mat3<T>(
					m[0][0] * rhs.m[0][0] + m[1][0] *rhs.m[0][1] + m[2][0] * rhs.m[0][2]
				, m[0][1] * rhs.m[0][0] + m[1][1] *rhs.m[0][1] + m[2][1] * rhs.m[0][2]
				, m[0][2] * rhs.m[0][0] + m[1][2] *rhs.m[0][1] + m[2][2] * rhs.m[0][2]
				, m[0][0] * rhs.m[1][0] + m[1][0] *rhs.m[1][1] + m[2][0] * rhs.m[1][2]
				, m[0][1] * rhs.m[1][0] + m[1][1] *rhs.m[1][1] + m[2][1] * rhs.m[1][2]
				, m[0][2] * rhs.m[1][0] + m[1][2] *rhs.m[1][1] + m[2][2] * rhs.m[1][2]
				, m[0][0] * rhs.m[2][0] + m[1][0] *rhs.m[2][1] + m[2][0] * rhs.m[2][2]
				, m[0][1] * rhs.m[2][0] + m[1][1] *rhs.m[2][1] + m[2][1] * rhs.m[2][2]
				, m[0][2] * rhs.m[2][0] + m[1][2] *rhs.m[2][1] + m[2][2] * rhs.m[2][2]);
	}

	Mat3<T> operator* (const T& scalar) const
	{
		return Mat3<T>(m[0][0] * scalar, m[0][1] * scalar, m[0][2] * scalar
				, m[1][0] * scalar, m[1][1] * scalar, m[1][2] * scalar
				, m[2][0] * scalar, m[2][1] * scalar, m[2][2] * scalar);
	}

	Mat3<T>& operator+= (const Mat3<T>& rhs)
	{
		m[0][0] + rhs.m[0][0]; m[0][1] + rhs.m[0][1]; m[0][2] + rhs.m[0][2];
		m[1][0] + rhs.m[1][0]; m[1][1] + rhs.m[1][1]; m[1][2] + rhs.m[1][2];
		m[2][0] + rhs.m[2][0]; m[2][1] + rhs.m[2][1]; m[2][2] + rhs.m[2][2];
		return *this;
	}

	Mat3<T>& operator-= (const Mat3<T>& rhs)
	{
		m[0][0] - rhs.m[0][0]; m[0][1] - rhs.m[0][1]; m[0][2] - rhs.m[0][2];
		m[1][0] - rhs.m[1][0]; m[1][1] - rhs.m[1][1]; m[1][2] - rhs.m[1][2];
		m[2][0] - rhs.m[2][0]; m[2][1] - rhs.m[2][1]; m[2][2] - rhs.m[2][2];
		return *this;
	}

	Mat3<T>& operator*= (const Mat3<T>& rhs)
	{
		*this = *this * rhs;
		return *this;
	}

	Mat3<T>& operator*= (const T& scalar)
	{
		m[0][0] * scalar; m[0][1] * scalar; m[0][2] * scalar;
		m[1][0] * scalar; m[1][1] * scalar; m[1][2] * scalar;
		m[2][0] * scalar; m[2][1] * scalar; m[2][2] * scalar;
		return *this;
	}

	bool operator== (const Mat3& rhs) const
	{
		return (m[0][0] == rhs.m[0][0] && m[0][1] == rhs.m[0][1] && m[0][2] == rhs.m[0][2]
				&& m[1][0] == rhs.m[1][0] && m[1][1] == rhs.m[1][1] && m[1][2] == rhs.m[1][2]
				&& m[2][0] == rhs.m[2][0] && m[2][1] == rhs.m[2][1] && m[2][2] == rhs.m[2][2]);
	}

	bool operator!= (const Mat3& other) { return !(*this == other); }

	Vec3<T> getRow(size_t n) const {return Vec3<T>(m[n][0],m[n][1],m[n][2]); }
	Vec3<T> getColumn(size_t n) const {return Vec3<T>(m[0][n],m[1][n],m[2][n]); }

	void setIdentity()
	{
		m[0][0] = 1; m[0][1] = 0; m[0][2] = 0;
		m[1][0] = 0; m[1][1] = 1; m[1][2] = 0;
		m[2][0] = 0; m[2][1] = 0; m[2][2] = 1;
	}

	T determinant()
	{
		return (+ m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
				- m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0])
				+ m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]));
	}

	Mat3<T> transpose()
	{
		return Mat3<T>(m[2][2], m[2][1], m[2][0]
				, m[1][2], m[1][1], m[1][0]
				, m[0][2], m[0][1], m[0][0]);
	}

	Mat3<T> inverse()
	{
		T OneOverDeterminant = static_cast<T>(1) / determinant();
		return Mat3<T>(+ (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * OneOverDeterminant
				, - (m[1][0] * m[2][2] - m[2][0] * m[1][2]) * OneOverDeterminant
				, + (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * OneOverDeterminant
				, - (m[0][1] * m[2][2] - m[2][1] * m[0][2]) * OneOverDeterminant
				, + (m[0][0] * m[2][2] - m[2][0] * m[0][2]) * OneOverDeterminant
				, - (m[0][0] * m[2][1] - m[2][0] * m[0][1]) * OneOverDeterminant
				, + (m[0][1] * m[1][2] - m[1][1] * m[0][2]) * OneOverDeterminant
				, - (m[0][0] * m[1][2] - m[1][0] * m[0][2]) * OneOverDeterminant
				, + (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * OneOverDeterminant);

	}

	T m[3][3];
};

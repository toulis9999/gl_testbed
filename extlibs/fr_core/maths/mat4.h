#pragma once

#include <assert.h>
#include <cstdlib>

#include "vec4.h"

template <typename T>
struct Mat4
{
public:
	Mat4() = default;
	Mat4(T _m00, T _m01, T _m02, T _m03, T _m10, T _m11, T _m12, T _m13, T _m20, T _m21, T _m22, T _m23, T _m30, T _m31, T _m32, T _m33)
	{
		m[0][0] = _m00; m[0][1] = _m01; m[0][2] = _m02; m[0][3] = _m03;
		m[1][0] = _m10; m[1][1] = _m11; m[1][2] = _m12; m[1][3] = _m13;
		m[2][0] = _m20; m[2][1] = _m21; m[2][2] = _m22; m[2][3] = _m23;
		m[3][0] = _m30; m[3][1] = _m31; m[3][2] = _m32; m[3][3] = _m33;
	}

	Mat4(const Vec4<T>& v0, const Vec4<T>& v1, const Vec4<T>& v2, const Vec4<T>& v3)
	{
		m[0][0] = v0.x; m[0][1] = v0.y; m[0][2] = v0.z; m[0][3] = v0.w;
		m[1][0] = v1.x; m[1][1] = v1.y; m[1][2] = v1.z; m[1][3] = v1.w;
		m[2][0] = v2.x; m[2][1] = v2.y; m[2][2] = v2.z; m[2][3] = v2.w;
		m[3][0] = v3.x; m[3][1] = v3.y; m[3][2] = v3.z; m[3][3] = v3.w;
	}

	Mat4<T> operator+ (const Mat4<T>& rhs) const
	{
		return Mat4<T>(m[0][0] + rhs.m[0][0], m[0][1] + rhs.m[0][1], m[0][2] + rhs.m[0][2], m[0][3] + rhs.m[0][3]
				, m[1][0] + rhs.m[1][0], m[1][1] + rhs.m[1][1], m[1][2] + rhs.m[1][2], m[1][3] + rhs.m[1][3]
				, m[2][0] + rhs.m[2][0], m[2][1] + rhs.m[2][1], m[2][2] + rhs.m[2][2], m[2][3] + rhs.m[2][3]
				, m[3][0] + rhs.m[3][0], m[3][1] + rhs.m[3][1], m[3][2] + rhs.m[3][2], m[3][3] + rhs.m[3][3]);
	}

	Mat4<T> operator- (const Mat4<T>& rhs) const
	{
		return Mat4<T>(m[0][0] - rhs.m[0][0], m[0][1] - rhs.m[0][1], m[0][2] - rhs.m[0][2], m[0][3] - rhs.m[0][3]
				, m[1][0] - rhs.m[1][0], m[1][1] - rhs.m[1][1], m[1][2] - rhs.m[1][2], m[1][3] - rhs.m[1][3]
				, m[2][0] - rhs.m[2][0], m[2][1] - rhs.m[2][1], m[2][2] - rhs.m[2][2], m[2][3] - rhs.m[2][3]
				, m[3][0] - rhs.m[3][0], m[3][1] - rhs.m[3][1], m[3][2] - rhs.m[3][2], m[3][3] - rhs.m[3][3]);
	}

	Mat4<T> operator* (const Mat4<T>& rhs) const
	{
		return Mat4<T>(
					m[0][0] * rhs.m[0][0] + m[1][0] * rhs.m[0][1] + m[2][0] * rhs.m[0][2] + m[3][0] * rhs.m[0][3]
				, m[0][1] * rhs.m[0][0] + m[1][1] * rhs.m[0][1] + m[2][1] * rhs.m[0][2] + m[3][1] * rhs.m[0][3]
				, m[0][2] * rhs.m[0][0] + m[1][2] * rhs.m[0][1] + m[2][2] * rhs.m[0][2] + m[3][2] * rhs.m[0][3]
				, m[0][3] * rhs.m[0][0] + m[1][3] * rhs.m[0][1] + m[2][3] * rhs.m[0][2] + m[3][3] * rhs.m[0][3]
				, m[0][0] * rhs.m[1][0] + m[1][0] * rhs.m[1][1] + m[2][0] * rhs.m[1][2] + m[3][0] * rhs.m[1][3]
				, m[0][1] * rhs.m[1][0] + m[1][1] * rhs.m[1][1] + m[2][1] * rhs.m[1][2] + m[3][1] * rhs.m[1][3]
				, m[0][2] * rhs.m[1][0] + m[1][2] * rhs.m[1][1] + m[2][2] * rhs.m[1][2] + m[3][2] * rhs.m[1][3]
				, m[0][3] * rhs.m[1][0] + m[1][3] * rhs.m[1][1] + m[2][3] * rhs.m[1][2] + m[3][3] * rhs.m[1][3]
				, m[0][0] * rhs.m[2][0] + m[1][0] * rhs.m[2][1] + m[2][0] * rhs.m[2][2] + m[3][0] * rhs.m[2][3]
				, m[0][1] * rhs.m[2][0] + m[1][1] * rhs.m[2][1] + m[2][1] * rhs.m[2][2] + m[3][1] * rhs.m[2][3]
				, m[0][2] * rhs.m[2][0] + m[1][2] * rhs.m[2][1] + m[2][2] * rhs.m[2][2] + m[3][2] * rhs.m[2][3]
				, m[0][3] * rhs.m[2][0] + m[1][3] * rhs.m[2][1] + m[2][3] * rhs.m[2][2] + m[3][3] * rhs.m[2][3]
				, m[0][0] * rhs.m[3][0] + m[1][0] * rhs.m[3][1] + m[2][0] * rhs.m[3][2] + m[3][0] * rhs.m[3][3]
				, m[0][1] * rhs.m[3][0] + m[1][1] * rhs.m[3][1] + m[2][1] * rhs.m[3][2] + m[3][1] * rhs.m[3][3]
				, m[0][2] * rhs.m[3][0] + m[1][2] * rhs.m[3][1] + m[2][2] * rhs.m[3][2] + m[3][2] * rhs.m[3][3]
				, m[0][3] * rhs.m[3][0] + m[1][3] * rhs.m[3][1] + m[2][3] * rhs.m[3][2] + m[3][3] * rhs.m[3][3]);
	}

	Mat4<T> operator* (const T& scalar) const
	{
		return Mat4<T>(m[0][0] * scalar, m[0][1] * scalar, m[0][2] * scalar, m[0][3] * scalar
				, m[1][0] * scalar, m[1][1] * scalar, m[1][2] * scalar, m[1][3] * scalar
				, m[2][0] * scalar, m[2][1] * scalar, m[2][2] * scalar, m[2][3] * scalar
				, m[3][0] * scalar, m[3][1] * scalar, m[3][2] * scalar, m[3][3] * scalar);
	}

	Mat4<T>& operator+= (const Mat4<T>& rhs)
	{
		m[0][0] + rhs.m[0][0]; m[0][1] + rhs.m[0][1]; m[0][2] + rhs.m[0][2]; m[0][3] + rhs.m[0][3];
		m[1][0] + rhs.m[1][0]; m[1][1] + rhs.m[1][1]; m[1][2] + rhs.m[1][2]; m[1][3] + rhs.m[1][3];
		m[2][0] + rhs.m[2][0]; m[2][1] + rhs.m[2][1]; m[2][2] + rhs.m[2][2]; m[2][3] + rhs.m[2][3];
		m[3][0] + rhs.m[3][0]; m[3][1] + rhs.m[3][1]; m[3][2] + rhs.m[3][2]; m[3][3] + rhs.m[3][3];
		return *this;
	}

	Mat4<T>& operator-= (const Mat4<T>& rhs)
	{
		m[0][0] - rhs.m[0][0]; m[0][1] - rhs.m[0][1]; m[0][2] - rhs.m[0][2]; m[0][3] - rhs.m[0][3];
		m[1][0] - rhs.m[1][0]; m[1][1] - rhs.m[1][1]; m[1][2] - rhs.m[1][2]; m[1][3] - rhs.m[1][3];
		m[2][0] - rhs.m[2][0]; m[2][1] - rhs.m[2][1]; m[2][2] - rhs.m[2][2]; m[2][3] - rhs.m[2][3];
		m[3][0] - rhs.m[3][0]; m[3][1] - rhs.m[3][1]; m[3][2] - rhs.m[3][2]; m[3][3] - rhs.m[3][3];
		return *this;
	}

	Mat4<T>& operator*= (const Mat4<T>& rhs)
	{
		*this = *this * rhs;
		return *this;
	}

	Mat4<T>& operator*= (const T& scalar)
	{
		m[0][0] * scalar; m[0][1] * scalar; m[0][2] * scalar; m[0][3] * scalar;
		m[1][0] * scalar; m[1][1] * scalar; m[1][2] * scalar; m[1][3] * scalar;
		m[2][0] * scalar; m[2][1] * scalar; m[2][2] * scalar; m[2][3] * scalar;
		m[3][0] * scalar; m[3][1] * scalar; m[3][2] * scalar; m[3][3] * scalar;
		return *this;
	}

	bool operator== (const Mat4& rhs) const
	{
		return (m[0][0] == rhs.m[0][0] && m[0][1] == rhs.m[0][1] && m[0][2] == rhs.m[0][2] && m[0][3] == rhs.m[0][3]
				&& m[1][0] == rhs.m[1][0] && m[1][1] == rhs.m[1][1] && m[1][2] == rhs.m[1][2] && m[1][3] == rhs.m[1][3]
				&& m[2][0] == rhs.m[2][0] && m[2][1] == rhs.m[2][1] && m[2][2] == rhs.m[2][2] && m[2][3] == rhs.m[2][3]
				&& m[3][0] == rhs.m[3][0] && m[3][1] == rhs.m[3][1] && m[3][2] == rhs.m[3][2] && m[3][3] == rhs.m[3][3]);
	}

	bool operator!= (const Mat4& other) { return !(*this == other); }

	Vec4<T> getRow(size_t n) const {return Vec4<T>(m[n][0],m[n][1],m[n][2],m[n][3]); }
	Vec4<T> getColumn(size_t n) const {return Vec4<T>(m[0][n],m[1][n],m[2][n],m[3][n]); }

	void setIdentity()
	{
		m[0][0] = 1; m[0][1] = 0; m[0][2] = 0; m[0][3] = 0;
		m[1][0] = 0; m[1][1] = 1; m[1][2] = 0; m[1][3] = 0;
		m[2][0] = 0; m[2][1] = 0; m[2][2] = 1; m[2][3] = 0;
		m[3][0] = 0; m[3][1] = 0; m[3][2] = 0; m[3][3] = 1;
	}

	T determinant()
	{
		T S00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
		T S01 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
		T S02 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
		T S03 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
		T S04 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
		T S05 = m[2][0] * m[3][1] - m[3][0] * m[2][1];

		Vec4<T> vDet(+(m[1][1] * S00 - m[1][2] * S01 + m[1][3] * S02)
				,-(m[1][0] * S00 - m[1][2] * S03 + m[1][3] * S04)
				,+(m[1][0] * S01 - m[1][1] * S03 + m[1][3] * S05)
				,-(m[1][0] * S02 - m[1][1] * S04 + m[1][2] * S05));

		return m[0][0] * vDet.x + m[0][1] * vDet.y + m[0][2] * vDet.z + m[0][3] * vDet.w;
	}

	Mat4<T> transpose()
	{
		return Mat4<T>(m[3][3], m[3][2], m[3][1], m[3][0]
				, m[2][3], m[2][2], m[2][1], m[2][0]
				, m[1][3], m[1][2], m[1][1], m[1][0]
				, m[0][3], m[0][2], m[0][1], m[0][0]);
	}

	Mat4<T> inverse()
	{
		T C00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
		T C02 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
		T C03 = m[1][2] * m[2][3] - m[2][2] * m[1][3];
		T C04 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
		T C06 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
		T C07 = m[1][1] * m[2][3] - m[2][1] * m[1][3];
		T C08 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
		T C10 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
		T C11 = m[1][1] * m[2][2] - m[2][1] * m[1][2];
		T C12 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
		T C14 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
		T C15 = m[1][0] * m[2][3] - m[2][0] * m[1][3];
		T C16 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
		T C18 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
		T C19 = m[1][0] * m[2][2] - m[2][0] * m[1][2];
		T C20 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
		T C22 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
		T C23 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

		Vec4<T> F0(C00, C00, C02, C03); Vec4<T> F1(C04, C04, C06, C07);
		Vec4<T> F2(C08, C08, C10, C11); Vec4<T> F3(C12, C12, C14, C15);
		Vec4<T> F4(C16, C16, C18, C19); Vec4<T> F5(C20, C20, C22, C23);

		Vec4<T> V0(m[1][0], m[0][0], m[0][0], m[0][0]);
		Vec4<T> V1(m[1][1], m[0][1], m[0][1], m[0][1]);
		Vec4<T> V2(m[1][2], m[0][2], m[0][2], m[0][2]);
		Vec4<T> V3(m[1][3], m[0][3], m[0][3], m[0][3]);

		Vec4<T> I0(V1 * F0 - V2 * F1 + V3 * F2); Vec4<T> I1(V0 * F0 - V2 * F3 + V3 * F4);
		Vec4<T> I2(V0 * F1 - V1 * F3 + V3 * F5); Vec4<T> I3(V0 * F2 - V1 * F4 + V2 * F5);
		Vec4<T> SignA(+1, -1, +1, -1); Vec4<T> SignB(-1, +1, -1, +1);
		Mat4<T> Inverse(I0 * SignA, I1 * SignB, I2 * SignA, I3 * SignB);

		Vec4<T> Row0(Inverse.m[0][0], Inverse.m[1][0], Inverse.m[2][0], Inverse.m[3][0]);

		Vec4<T> Dot0( Vec4<T>(m[0][0], m[0][1], m[0][2], m[0][3]) * Row0);
		T Dot1 = (Dot0.x + Dot0.y) + (Dot0.z + Dot0.w);

		T OneOverDeterminant = static_cast<T>(1) / Dot1;

		return Inverse * OneOverDeterminant;
	}

	T m[4][4];
};

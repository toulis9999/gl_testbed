#pragma once

#include <type_traits> // is_arithmetic
#include <stdlib.h> //size_t

#include <math.h>
#include "mat4.h"
#include "mat3.h"
#include "vec3.h"

template <typename T>
struct Quaternion
{
	//limit T to numeric types only
	static_assert(std::is_arithmetic<T>::value, "Type of Quaternion MUST be Numeric");

	Quaternion() = default;
	Quaternion(T _x, T _y, T _z, T _w) : x(_x), y(_y), z(_z), w(_w) {}

	//binary operand ops
	Quaternion<T> operator+ (const Quaternion<T>& rhs) const { x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w; }
	Quaternion<T> operator- (const Quaternion<T>& rhs) const { x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w; }
	Quaternion<T> operator* (const Quaternion<T>& rhs) const
	{
		return Quaternion<T>{w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y,
			w * rhs.y + y * rhs.w + z * rhs.x - x * rhs.z,
			w * rhs.z + z * rhs.w + x * rhs.y - y * rhs.x,
			w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z};
	}

	Quaternion<T> operator* (const T& scalar) const { return Quaternion<T>(x * scalar, y * scalar, z * scalar, w * scalar); }

	//unary operand ops
	Quaternion<T>& operator+= (const Quaternion<T>& other)
	{
		x += other.x; y += other.y; z += other.z; w += other.w;
		return *this;
	}

	Quaternion& operator-= (const Quaternion<T>& other)
	{
		x -= other.x; y -= other.y; z -= other.z; w -= other.w;
		return *this;
	}

	Quaternion& operator*= (const Quaternion<T>& other)
	{
		Quaternion<T> tmp(*this);
		x = tmp.w * other.x + tmp.x * other.w + tmp.y * other.z - tmp.z * other.y;
		y = tmp.w * other.y + tmp.y * other.w + tmp.z * other.x - tmp.x * other.z;
		z = tmp.w * other.z + tmp.z * other.w + tmp.x * other.y - tmp.y * other.x;
		w = tmp.w * other.w - tmp.x * other.x - tmp.y * other.y - tmp.z * other.z;
		return *this;
	}

	Quaternion<T>& operator*= (const T& scalar)
	{
		x *= scalar; y *= scalar; z *= scalar; w *= scalar;
		return *this;
	}

	bool operator== (const Quaternion& other) const { return x == other.x &&	y == other.y && z == other.z && w == other.w; }
	bool operator!= (const Quaternion& other) const { return !(*this == other); }

	void setIdentity() { x = 0; y = 0; z = 0; w = 1; }

	T length() const { return sqrt((x * x) + (y * y) + (z * z) + (w * w)); }
	T lengthSq() const { return (x * x) + (y * y) + (z * z) + (w * w); }

	void conjugate() { x = -x; y = -y; z = -z; }
	void inverse()
	{
		conjugate();
		T oneOverDot = 1 / lengthSq();
		x *= oneOverDot; y *= oneOverDot; z *= oneOverDot; w *= oneOverDot;
	}

	Mat3<T> toMat3() const
	{
		T xx = x * x; T yy = y * y; T zz = z * z;
		T xy = x * y; T xz = x * z;	T xw = x * w;
		T yz = y * z; T yw = y * w;	T zw = z * w;
		return Mat3<T>{1 - 2 * (yy + zz), 2 * (xy + zw), 2 * (xz - yw),
			2 * (xy - zw), 1 - 2 * (xx + zz), 2 * (yz + xw),
			2 * (xz + yw), 2 * (yz - xw), 1 - 2 * (xx + yy)};
	}

	Mat4<T> toMat4() const
	{
		T xx = x * x; T yy = y * y; T zz = z * z;
		T xy = x * y; T xz = x * z;	T xw = x * w;
		T yz = y * z; T yw = y * w;	T zw = z * w;
		return Mat4<T>{1 - 2 * (yy + zz), 2 * (xy + zw), 2 * (xz - yw), 0,
			2 * (xy - zw), 1 - 2 * (xx + zz), 2 * (yz + xw), 0,
			2 * (xz + yw), 2 * (yz - xw), 1 - 2 * (xx + yy), 0,
			0, 0, 0, 1};
	}

	T x, y, z, w;
};

template<typename T>
static Quaternion<T> crossProduct(const Quaternion<T>& lhs, const Quaternion<T>& rhs)
{
	return 	Quaternion<T>{lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y,
		lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
		lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
		lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z, };
}

template<typename T>
static T dotProduct(const Quaternion<T>& lhs, const Quaternion<T>& rhs)
{
	return 	(((lhs.x * rhs.x) + (lhs.y * rhs.y)) + (lhs.z * rhs.z)) + (lhs.w * rhs.w);
}

template<typename T>
static Quaternion<T> lerp(const Quaternion<T>& lhs, const Quaternion<T>& rhs, float a)
{
	return Quaternion<T>(lerp(lhs.x, rhs.x, a),
		lerp(lhs.y, rhs.y, a),
		lerp(lhs.z, rhs.z, a),
		lerp(lhs.w, rhs.w, a));
}

template<typename T>
static Quaternion<T> slerp(const Quaternion<T>& lhs, const Quaternion<T>& rhs, float a)
{
	//Both Quaternions Should be normalised or garbage data will be output
	T cosTheta = dotProduct(lhs, rhs);
	T angle = acos(cosTheta);
	auto q = sin((1 - a) * angle);
	auto w = sin(a * angle);
	auto e = sin(angle);
	assert(e != 0); // angle between quaternions too low, consider lerp
	return Quaternion<T>((q * lhs.x + w * rhs.x) / e,
		(q * lhs.y + w * rhs.y) / e,
		(q * lhs.z + w * rhs.z) / e,
		(q * lhs.w + w * rhs.w) / e);
}

template<typename T>
Vec3<T> operator* (const Quaternion<T>& lhs, const Vec3<T>& rhs)
{
	Vec3<T> tmp{ lhs.x, lhs.y, lhs.z };
	Vec3<T> t = crossProduct(tmp, rhs);
	Vec3<T> r = crossProduct(tmp, t);
	return rhs + ((t* lhs.w) + r) * 2;
}

template <typename T>
Quaternion<T> normalise(const Quaternion<T>& q)
{
	auto oneOverLength = 1 / q.length();
	return Quaternion<T>(q.x * oneOverLength, q.y * oneOverLength, q.z * oneOverLength, q.w * oneOverLength);
}

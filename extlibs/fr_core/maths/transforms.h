#pragma once

#include "mat3.h"
#include "mat4.h"
#include "vec3.h"
#include "vec4.h"
#include "quaternion.h"

template<typename T>
Mat4<T> lookAt(const Vec3<T>& eye, const Vec3<T>& target, const Vec3<T>& up)
{
	Vec3<T> zaxis = normalise(eye - target);
	Vec3<T> xaxis = normalise(crossProduct(up, zaxis));
	Vec3<T> yaxis = crossProduct(zaxis, xaxis);

	Mat4<T> viewMat{ xaxis.x, yaxis.x, zaxis.x, 0,
						 xaxis.y, yaxis.y, zaxis.y, 0,
						 xaxis.z, yaxis.z, zaxis.z, 0,
						 -dotProduct(xaxis, eye), -dotProduct(yaxis, eye), -dotProduct(zaxis, eye), 1 };

	return viewMat;
}

template<typename T>
Mat4<T> perspective(const T& fovInRadians, const T& aspect, const T& nearZ, const T& farZ)
{
	assert(aspect != 0);
	assert(farZ != nearZ);

	T const tanHalfFov = tan(fovInRadians * static_cast<T>(0.5));

	Mat4<T> persp{ 1 / (aspect * tanHalfFov),0,0,0,
		0, 1 / (tanHalfFov),0,0,
		0,0, -(farZ + nearZ) / (farZ - nearZ), -1,
		0,0, -(2 * farZ * nearZ) / (farZ - nearZ),0 };
	return persp;
}

template<typename T>
Mat4<T> ortho(const T& left, const T& right, const T& bottom, const T& top, const T& nearZ, const T& farZ)
{
	Mat4<T> res{ 2 / (right - left),0,0,0,
					 0,2 / (top - bottom),0,0,
					 0,0,-2 / (farZ - nearZ),0,
		-(right + left) / (right - left),-(top + bottom) / (top - bottom),-(farZ + nearZ) / (farZ - nearZ),1 };
	return res;
}

template<typename T>
Mat4<T> translate(const T& x, const T& y, const T& z)
{
	Mat4<T> mat{ 1, 0, 0, 0,
					 0, 1, 0, 0,
					 0, 0, 1, 0,
					 x, y, z, 1 };
	return mat;
}

template<typename T>
Mat4<T> scale(const T& x, const T& y, const T& z)
{
	Mat4<T> mat{ x, 0, 0, 0,
					 0, y, 0, 0,
					 0, 0, z, 0,
					 0, 0, 0, 1 };
	return mat;
}

template<typename T>
Mat4<T> rotate(const T& angleInRadians, const Vec3<T>& axis)
{
	const T c = cos(angleInRadians);
	const T s = sin(angleInRadians);

	Vec3<T> naxis(normalise(axis));
	Vec3<T> temp(naxis * (T(1) - c));

	Mat4<T> rot{ c + temp.x * naxis.x,	temp.x * naxis.y + s * naxis.z,	temp.x * naxis.z - s * naxis.y,0,
					 temp.y * naxis.x - s * naxis.z, c + temp.y * naxis.y, temp.y * naxis.z + s * naxis.x,0,
					 temp.z * naxis.x + s * naxis.y, temp.z * naxis.y - s * naxis.x, c + temp.z * naxis.z,0,0,0,0,1 };
	return rot;
}

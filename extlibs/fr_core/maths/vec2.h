#pragma once

#include <type_traits> // is_arithmetic
#include <cstdlib> //size_t
#include <math.h>

template <typename T>
struct Vec2
{
	//limit T to numeric types only
	static_assert(std::is_arithmetic<T>::value, "Type of Vector MUST be Numeric");
public:
	typedef T value_type;

	Vec2() = default;
	Vec2(T _x, T _y) : x(_x), y(_y) {}

	bool operator== (const Vec2& other) const {return (x == other.x)&&(y == other.y);}
	bool operator!= (const Vec2& other) const {return !(*this == other);}

	Vec2<T> operator+ (const Vec2<T>& rhs) const { return Vec2(x + rhs.x, y + rhs.y); }
	Vec2<T> operator- (const Vec2<T>& rhs) const { return Vec2(x - rhs.x, y - rhs.y); }
	Vec2<T> operator* (const Vec2<T>& rhs) const { return Vec2(x * rhs.x, y * rhs.y); }
	Vec2<T> operator* (const T& scalar) const { return Vec2(x * scalar, y *scalar); }

	Vec2& operator+= (const Vec2<T>& other)
	{
		x += other.x; y += other.y;
		return *this;
	}
	
	Vec2& operator-= (const Vec2<T>& other)
	{
		x -= other.x; y -= other.y;
		return *this;
	}

	Vec2& operator*= (const Vec2<T>& other)
	{
		x *= other.x; y *= other.y;
		return *this;
	}
	
	Vec2& operator*= (const T& scalar)
	{
		x *= scalar; y *= scalar;
		return *this;
	}

	T length() const { return sqrt(this->lengthSq()); }
	T lengthSq() const { return (x * x + y * y); }

	T x, y;
};

template<typename T>
Vec2<T> operator- (const Vec2<T>& rhs)
{
	return Vec2<T>(-rhs.x, -rhs.y);
}

template <typename T>
Vec2<T> operator*(T lhs, Vec2<T> const& rhs)
{
	return Vec2<T>(lhs * rhs.x, lhs * rhs.y);
}

template <typename T>
static T dotProduct(const Vec2<T>& lhs, const Vec2<T>& rhs)
{
	return (lhs.x * rhs.x) + (lhs.y * rhs.y);
}

template <typename T>
Vec2<T> normalise(const Vec2<T>& v)
{
	auto oneOverLength = 1 / v.length();
	return Vec2<T>(v.x * oneOverLength, v.y * oneOverLength);
}

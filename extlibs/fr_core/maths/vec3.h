#pragma once

#include <type_traits> // is_arithmetic
#include <cstdlib> //size_t
#include <math.h>

template <typename T>
struct Mat3;

template <typename T>
struct Vec3
{
	//limit T to numeric types only
	static_assert(std::is_arithmetic<T>::value, "Type of Vector MUST be Numeric");
public:
	typedef T value_type;

	Vec3() = default;
	Vec3(T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}

	bool operator== (const Vec3& other) const {return (x == other.x)&&(y == other.y)&&(z == other.z);}
	bool operator!= (const Vec3& other) const {return !(*this == other);}

	Vec3<T> operator+ (const Vec3<T>& rhs) const { return Vec3(x + rhs.x, y + rhs.y, z + rhs.z); }
	Vec3<T> operator- (const Vec3<T>& rhs) const { return Vec3(x - rhs.x, y - rhs.y, z - rhs.z); }
	Vec3<T> operator* (const Vec3<T>& rhs) const { return Vec3(x * rhs.x, y * rhs.y, z * rhs.z); }
	Vec3<T> operator* (const T& scalar) const { return Vec3(x * scalar, y *scalar, z * scalar); }

	Vec3& operator+= (const Vec3<T>& other)
	{
		x += other.x; y += other.y; z += other.z;
		return *this;
	}
	
	Vec3& operator-= (const Vec3<T>& other)
	{
		x -= other.x; y -= other.y; z -= other.z;
		return *this;
	}

	Vec3& operator*= (const Vec3<T>& other)
	{
		x *= other.x; y *= other.y; z *= other.z;
		return *this;
	}
	
	Vec3& operator*= (const T& scalar)
	{
		x *= scalar; y *= scalar; z *= scalar;
		return *this;
	}

	Vec3& operator*= (const Mat3<T>& other)
	{
		auto tmpX = x * other.m[0][0] + y * other.m[0][1] + z * other.m[0][2];
		auto tmpY = x * other.m[1][0] + y * other.m[1][1] + z * other.m[1][2];
		auto tmpZ = x * other.m[2][0] + y * other.m[2][1] + z * other.m[2][2];
		x = tmpX; y = tmpY; z = tmpZ;
		return *this;
	}

	T length() const { return sqrt(this->lengthSq()); }
	T lengthSq() const { return (x * x + y * y + z * z); }

	T x, y, z;
};

template<typename T>
Vec3<T> operator- (const Vec3<T>& rhs)
{
	return Vec3<T>(-rhs.x, -rhs.y, -rhs.z);
}

template <typename T>
Vec3<T> operator*(T lhs, Vec3<T> const& rhs)
{
	return Vec3<T>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
}

template<typename T>
Vec3<T> operator*(const Mat3<T>& lhs, const Vec3<T>& rhs)
{
	return Vec3<T>(rhs.x * lhs.m[0][0] + rhs.y * lhs.m[1][0] + rhs.z * lhs.m[2][0]
		, rhs.x * lhs.m[0][1] + rhs.y * lhs.m[1][1] + rhs.z * lhs.m[2][1]
		, rhs.x * lhs.m[0][2] + rhs.y * lhs.m[1][2] + rhs.z * lhs.m[2][2]);
}

template<typename T>
Vec3<T> operator*(const Vec3<T>& lhs, const Mat3<T>& rhs)
{
	return Vec3<T>(lhs.x * rhs.m[0][0] + lhs.y * rhs.m[0][1] + lhs.z * rhs.m[0][2]
		, lhs.x * rhs.m[1][0] + lhs.y * rhs.m[1][1] + lhs.z * rhs.m[1][2]
		, lhs.x * rhs.m[2][0] + lhs.y * rhs.m[2][1] + lhs.z * rhs.m[2][2]);
}

template <typename T>
static T dotProduct(const Vec3<T>& lhs, const Vec3<T>& rhs)
{
	return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z);
}

template<typename T>
static Vec3<T> crossProduct(const Vec3<T>& lhs, const Vec3<T>& rhs)
{
	return Vec3<T>(lhs.y * rhs.z - lhs.z * rhs.y
		,lhs.z * rhs.x - lhs.x * rhs.z
		,lhs.x * rhs.y - lhs.y  * rhs.x);
}

template <typename T>
Vec3<T> normalise(const Vec3<T>& v)
{
	auto oneOverLength = 1 / v.length();
	return Vec3<T>(v.x * oneOverLength, v.y * oneOverLength, v.z * oneOverLength);
}

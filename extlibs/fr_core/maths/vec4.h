#pragma once

#include <type_traits> // is_arithmetic
#include <cstdlib> //size_t

template <typename T>
struct Mat4;

template <typename T>
struct Vec4
{
	//limit T to numeric types only
	static_assert(std::is_arithmetic<T>::value, "Type of Vector MUST be Numeric");
public:
	typedef T value_type;

	Vec4() = default;
	Vec4(T _x, T _y, T _z, T _w) : x(_x), y(_y), z(_z), w(_w) {}

	bool operator== (const Vec4& other) const {return (x == other.x)&&(y == other.y)&&(z == other.z) && (w == other.w); }
	bool operator!= (const Vec4& other) const {return !(*this == other);}

	Vec4<T> operator+ (const Vec4<T>& rhs) const { return Vec4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w); }
	Vec4<T> operator- (const Vec4<T>& rhs) const { return Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w); }
	Vec4<T> operator* (const Vec4<T>& rhs) const { return Vec4(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w); }
	Vec4<T> operator* (const T& scalar) const { return Vec4(x * scalar, y *scalar, z * scalar, w *scalar); }

	Vec4& operator+= (const Vec4<T>& other)
	{
		x += other.x; y += other.y; z += other.z; w += other.w;
		return *this;
	}
	
	Vec4& operator-= (const Vec4<T>& other)
	{
		x -= other.x; y -= other.y; z -= other.z; w -= other.w;
		return *this;
	}

	Vec4& operator*= (const Vec4<T>& other)
	{
		x *= other.x; y *= other.y; z *= other.z; w *= other.w;
		return *this;
	}
	
	Vec4& operator*= (const T& scalar)
	{
		x *= scalar; y *= scalar; z *= scalar; w *= scalar;
		return *this;
	}

	Vec4<T>& operator*= (const Mat4<T>& other)
	{
		auto tmpX = x * other.m[0][0] + y * other.m[0][1] + z * other.m[0][2] + w * other.m[0][3];
		auto tmpY = x * other.m[1][0] + y * other.m[1][1] + z * other.m[1][2] + w * other.m[1][3];
		auto tmpZ = x * other.m[2][0] + y * other.m[2][1] + z * other.m[2][2] + w * other.m[2][3];
		auto tmpW = x * other.m[3][0] + y * other.m[3][1] + z * other.m[3][2] + w * other.m[3][3];
		x = tmpX; y = tmpY; z = tmpZ; w = tmpW;
		return *this;
	}

	T length() const { return sqrt(this->lengthSq()); }
	T lengthSq() const { return (x * x + y * y + z * z + w * w); }

	T x, y, z, w;
};

template<typename T>
Vec4<T> operator- (const Vec4<T>& rhs)
{
	return Vec4<T>(-rhs.x, -rhs.y, -rhs.z, -rhs.w);
}

template <typename T>
Vec4<T> operator*(T lhs, Vec4<T> const& rhs)
{
	return Vec4<T>(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w);
}

template<typename T>
Vec4<T> operator*(const Mat4<T>& lhs, const Vec4<T>& rhs)
{
	return Vec4<T>(
		rhs.x * lhs.m[0][0] + rhs.y * lhs.m[1][0] + rhs.z * lhs.m[2][0] + rhs.w * lhs.m[3][0]
		, rhs.x * lhs.m[0][1] + rhs.y * lhs.m[1][1] + rhs.z * lhs.m[2][1] + rhs.w * lhs.m[3][1]
		, rhs.x * lhs.m[0][2] + rhs.y * lhs.m[1][2] + rhs.z * lhs.m[2][2] + rhs.w * lhs.m[3][2]
		, rhs.x * lhs.m[0][3] + rhs.y * lhs.m[1][3] + rhs.z * lhs.m[2][3] + rhs.w * lhs.m[3][3]);
}

template<typename T>
Vec4<T> operator*(const Vec4<T>& lhs, const Mat4<T>& rhs)
{
	return Vec4<T>(
		lhs.x * rhs.m[0][0] + lhs.y * rhs.m[0][1] + lhs.z * rhs.m[0][2] + lhs.w * rhs.m[0][3]
		, lhs.x * rhs.m[1][0] + lhs.y * rhs.m[1][1] + lhs.z * rhs.m[1][2] + lhs.w * rhs.m[1][3]
		, lhs.x * rhs.m[2][0] + lhs.y * rhs.m[2][1] + lhs.z * rhs.m[2][2] + lhs.w * rhs.m[2][3]
		, lhs.x * rhs.m[3][0] + lhs.y * rhs.m[3][1] + lhs.z * rhs.m[3][2] + lhs.w * rhs.m[3][3]);
}

template <typename T>
static T dotProduct(const Vec4<T>& lhs, const Vec4<T>& rhs)
{
	return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z) + (lhs.w * rhs.w);
}

template <typename T>
Vec4<T> normalise(const Vec4<T>& v)
{
	auto oneOverLength = 1 / v.length();
	return Vec4<T>(v.x * oneOverLength, v.y * oneOverLength, v.z * oneOverLength, v.w * oneOverLength);
}

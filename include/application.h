#ifndef APP_H
#define APP_H

#include <memory>
#include <string>
#include <vector>

#include <SDL.h>

#include "extlibs/glfuncs/glad.h"
#include "extlibs/fr_core/maths/fr_math.h"

#include "include/settings.h"
#include "scene.h"


class Application
{
public:
	Application();
	~Application();

	void insertScene(Scene* sc);
	void run();

private:
	bool initialise();


	bool running;
	SDL_Event event;

	Settings settings;

	std::vector<std::unique_ptr<Scene> > states;
	unsigned int currentScene; // index to currently active scene

	std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> mp_window;
	SDL_GLContext m_glcontext;

	const unsigned long long dt = (1.0f/60.0f) * SDL_GetPerformanceFrequency();

	unsigned long long newtime;
	unsigned long long frametime;
	unsigned long long t;

};

#endif // APP_H

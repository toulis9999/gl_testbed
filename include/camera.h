#ifndef CAMERA_H
#define CAMERA_H

#include "extlibs/fr_core/maths/fr_math.h"

class Camera
{
public:
	Camera();
	const vec3f& position() const;
	void setPosition(const vec3f& position);
	void offsetPosition(const vec3f& offset);

	float fieldOfView() const;
	void setFieldOfView(float fieldOfView);

	float nearPlane() const;
	float farPlane() const;

	void setNearAndFarPlanes(float nearPlane, float farPlane);

	mat4f orientation() const;

	void offsetOrientation(float upAngle, float rightAngle);

	void lookAt(vec3f position);

	float viewportAspectRatio() const;
	void setViewportAspectRatio(float viewportAspectRatio);

	vec3f forward() const;
	vec3f right() const;
	vec3f up() const;

	mat4f matrix() const;
	mat4f projection() const;
	mat4f view() const;

private:
	vec3f m_position;
	float m_horizontalAngle;
	float m_verticalAngle;
	float m_fieldOfView;
	float m_nearPlane;
	float m_farPlane;
	float m_viewportAspectRatio;
	void normalizeAngles();
};


#endif // CAMERA_H

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <vector>
#include <array>

#include "extlibs/glfuncs/glad.h"
#include "include/Texture.h"

class Framebuffer
{
public:
	Framebuffer();
	~Framebuffer();

	void attachTexture(const Texture& tex, GLenum attachPoint);

	void clear();

	void bind();
	void unbind();

	bool isBound() const;

private:
	bool bound;
	GLuint handle;
	int width, height;

	std::array <GLint, 4> OrigViewDims;
	std::vector<GLenum> attachments;
};

#endif // FRAMEBUFFER_H

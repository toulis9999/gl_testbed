#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>

#include "extlibs/glfuncs/glad.h"


class Mesh
{
public:
	Mesh();
	~Mesh();

	void loadFromFile(const std::string& fileName);
	void bind();
	void unbind();
	void draw();

private:
	GLuint vbo_pos_handle;
	GLuint vbo_uv_handle;
	GLuint vbo_normals_handle;
	GLuint vao_handle;

	size_t num_tris;
	bool bound;

	bool has_uvs;
	bool has_normals;
};

#endif // MESH_H

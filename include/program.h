#pragma once

#include <unordered_map>

#include "extlibs/fr_core/maths/fr_math.h"

class Program final
{
public:
	Program() : m_created(false), m_handle(0), m_bound(false) {}
	~Program();
	Program(const Program&) = delete;
	Program& operator=(const Program&) = delete;

	bool create(uint32_t vertex_shader_handle, uint32_t pixel_shader_handle);

	void bind();
	void unbind();

	int getErrorLength();
	void getLastError(char* out);

	int getHandle() const { return m_handle; }
	bool isCreated() const { return m_created; }
	bool isBound() const { return m_bound; }

	//the result is saved on uniformlocations map;
	void setUniform(const char* name, const vec2f& v);
	void setUniform(const char* name, const vec3f& v);
	void setUniform(const char* name, const vec4f& v);
	void setUniform(const char* name, const mat4f& m);
	void setUniform(const char* name, const mat3f& m);
	void setUniform(const char* name, float val);
	void setUniform(const char* name, int val);
	void setUniform(const char* name, bool val);

private:
	int getUniformLocation(const char* name);

	bool m_created;
	bool m_bound;
	uint32_t m_handle;
	std::unordered_map<const char*, int> m_uniformLocations;
};

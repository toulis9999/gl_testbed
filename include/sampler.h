#pragma once

#include <stdint.h>

enum FilteringMode
{
	NEAREST,
	LINEAR,
	NEAREST_MIP_NEAREST,
	LINEAR_MIP_NEAREST,
	NEAREST_MIP_LINEAR,
	LINEAR_MIP_LINEAR
};

enum RepeatMode
{
	REPEAT,
	MIRROR_REPEAT,
	CLAMP_EDGE,
};

class Sampler final
{
public:
	Sampler();
	~Sampler();

	Sampler(const Sampler&) = delete;
	Sampler& operator=(const Sampler&) = delete;

	uint32_t getHandle() const { return m_handle; }

	/*The object holds no state on where it is bound
	 * the binding management be done explicitly by the user
	 * due to one sampler being able to be bound to multiple units
	 */
	void bind(uint32_t TextureUnit);
	void unbind(uint32_t TextureUnit);

	FilteringMode getMinFilter() const { return m_minFilter; }
	void setMinFilter(const FilteringMode& value);

	FilteringMode getMagFilter() const { return m_magFilter; }
	void setMagFilter(const FilteringMode& value);

	RepeatMode getEdgeWrapU() const { return m_edgeWrapU; }
	void setEdgeWrapU(const RepeatMode& value);

	RepeatMode getEdgeWrapV() const { return m_edgeWrapV; }
	void setEdgeWrapV(const RepeatMode& value);

	RepeatMode getEdgeWrapW() const { return m_edgeWrapW; }
	void setEdgeWrapW(const RepeatMode& value);

protected:
	uint32_t m_handle;
	FilteringMode m_minFilter;
	FilteringMode m_magFilter;
	RepeatMode m_edgeWrapU;
	RepeatMode m_edgeWrapV;
	RepeatMode m_edgeWrapW;
};

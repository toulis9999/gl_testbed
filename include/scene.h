#ifndef SCENE_H
#define SCENE_H

#include <SDL.h>
#include "settings.h"


class Scene
{
public:
	Scene() {}
	virtual ~Scene(){}

	virtual void init() = 0;
	virtual void handleEvents(const SDL_Event& event) = 0;
	virtual void updateFixed(unsigned long long dt) = 0;
	virtual void update(unsigned long long dt) = 0;
	virtual void draw(unsigned long long dt) = 0;

	const Settings* settings;
};

#endif // SCENE_H

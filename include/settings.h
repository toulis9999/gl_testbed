#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>

class Settings final
{
public:
	Settings();
	void loadFromFile(const std::string& fileName);
	int getWindowHeight() const;
	int getWindowWidth() const;
	float getAspect() const;
	bool getVsync() const;
	std::string getWindowTitle() const;
	std::string getResourcesFolder() const;

private:
	int win_height;
	int win_width;
	float ratio;
	std::string window_title;
	std::string resources_folder;
	bool vsync;
};

#endif // SETTINGS_H

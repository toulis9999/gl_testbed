#pragma once

#include <stdint.h>

enum class ShaderType
{
	VERTEX,
	FRAGMENT
};

class Shader final
{
public:
	Shader(): m_handle(0) {}
	~Shader();
	Shader(const Shader&) = delete;
	Shader& operator=(const Shader&) = delete;

	bool compileShader(const char* fileName, ShaderType t);
	int getErrorLength();
	void getLastError(char* out);

	uint32_t getHandle() const { return m_handle; }
	ShaderType getType() const { return m_type; }
private:
	uint32_t m_handle;
	ShaderType m_type;
};

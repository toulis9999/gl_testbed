#pragma once

#include <stdint.h>

enum TextureType
{
	TEXTURE_1D,
	TEXTURE_2D,
	TEXTURE_3D,
	TEXTURE_CUBE
};

enum TextureFormat
{
	RGBA_8888
};

class Texture
{
public:
	Texture() : m_mipmapLevels(0), m_texUnit(-1), m_handle(0), m_bound(false) {}
	~Texture();

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

	void createFromFile2D(const char* fileName);
	void createFromFilesCube(const char* fName1, const char* fName2, const char* fName3,
		const char* fName4, const char* fName5, const char* fName6);

	void createEmpty2D(int w, int h, bool depth = false);
	void createEmptyCube(int w, int h);

	void generateMipChain();

	int getWidth() const { return m_width; }
	int getHeight() const { return m_height; }

	uint32_t getHandle() const { return m_handle; }
	int getUnit() const { return m_texUnit; }
	int getMipmapLevels() const { return m_mipmapLevels; }

	void bind(int Activeunit);
	void unbind();

protected:
	int m_mipmapLevels;
	int m_width, m_height;
	int m_texUnit;
	uint32_t m_handle;

	TextureType m_type;
	bool m_bound;
};

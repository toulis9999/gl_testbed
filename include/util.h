#ifndef UTIL_H
#define UTIL_H

#include <string>
#include "extlibs/glfuncs/glad.h"

namespace util
{
	bool fileExists (const std::string& name);
	bool screenGrab(int w, int h, const char* filename, bool flipY);
}

#endif // UTIL_H

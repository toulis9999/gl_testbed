# Simple OpenGL Framework #

An easy to use Opengl Framework with all the nessecary classes to get you started writing your own OpenGL apps easily

## Features ##

* stb_image library for loading images
* Wavefront obj loader
* Convenience classes for:
    * Shaders
    * Shader Programs
    * Textures - which can be loaded from images or created as empty:** see below**
    * Texture Cubemaps
    * Sampler Objects to manage Texture Sampling Parameters
    * Texture Mip Map chain generation
    * 3d Camera 
    * Framebuffer for offscreen rendering which can:
        * Get textures attached to it for writting which can then be used for sampling from on following drawcalls
* An Application framework managing the scenes you create featuring
    * Input Handling
    * Fixed interval update (at 60ticks per second)
    * Variable interval update

## Dependencies##
* A modern C++11 compiler with support for
    * std::uniqueptr
    * std::move
    * Range based for loops
* SDL2

## Dependencies##
* An Opengl 3.3 capable graphics card 
* Minimum screen resolution 800 x 600

## Build Instructions ##
* Download or build SDL2 from source
    * Windows - download [here](https://www.libsdl.org/release/SDL2-devel-2.0.3-VC.zip)
    * Linux - grab the from the Mercurial repository [link](http://hg.libsdl.org/SDL) and build using CMake

### Windows ###
1. Once you have downloaded and extracted the SDL2 development binaries copy the **lib** folder into **(Project Directory)/extlibs/SDL2-2.0.3/**
2. Open Visual Studio Solution and build the project and run
3. Don't forget to copy the SDL2.dll to the folder of the executable
4. In order to get the correct path to the resources folder, go to: 
**project->properties->configuration properties->Debugging**
and change the **Working Directory** to **$(SolutionDir)$(Platform)\$(Configuration)\\**

## Using the Framework ##
Please see example scene [here](https://bitbucket.org/toulis9999/gl_testbed/src/ea5522c22f35420cf619f170cd2153db0f0788fd/test_scenes/?at=master)

* Create a class that derives from Scene and implement the virtual methods
* On your main function create
    * an instance of the Application class
    * a pointer instance of your new class
* Use the **Application::insertscene** method to add the scene to your application
* Use the **Application::run** method which will runn the rendering and update loop

###Example:###

```
#!c++

Application app;
app.insertScene(new MyDerivedScene);
app.run();
```


## Navigating the example scene ##
* Use "w,a,s,d" keys to move around
* Use "z" to descend "x" to ascend
* Use alt+F4 to exit
* Use the mouse to look around
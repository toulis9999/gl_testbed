#version 330 core

in vec2 UV;
in vec3 Normals;

layout (location=0) out vec4 FragColor;

uniform sampler2D tex;
uniform sampler2D tex2;

void main()
{
	vec4 currentcol1 = texture(tex, vec2(UV.x, UV.y));
	vec4 red = vec4(1,0,0,1);
	//FragColor = vec4(abs(Normals.x),abs(Normals.y),abs(Normals.z),0);
	FragColor = currentcol1;
}
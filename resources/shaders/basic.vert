#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

uniform mat4 camera;

out vec2 UV;
out vec3 Norm;

void main()
{
	Norm = Normals;
	UV = Texcoord;
	gl_Position = camera * vec4(VertexPosition,1.0);
}
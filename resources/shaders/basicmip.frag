#version 330 core

in vec3 pos_eye;
in vec3 n_eye;

layout (location=0) out vec4 FragColor;

uniform mat4 View;
uniform float lvls;
uniform samplerCube tex;

void main()
{
	vec3 incident_eye = normalize (pos_eye);
	vec3 normal = normalize (n_eye);

	vec3 reflected = reflect (incident_eye, normal);
	// convert from eye to world space
	reflected = vec3 (inverse (View) * vec4 (reflected, 0.0));

	FragColor = textureLod(tex, reflected, lvls);
}
#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

uniform mat4 Proj;
uniform mat4 View;

out vec3 pos_eye;
out vec3 n_eye;

void main()
{
	pos_eye = vec3 (View * vec4 (VertexPosition, 1.0));
	n_eye = vec3 (View * vec4 (Normals, 0.0));
	gl_Position = Proj * View * vec4 (VertexPosition, 1.0);
}
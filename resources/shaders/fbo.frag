#version 330 core

in vec2 UV;
in vec4 Norm;
in float Depth;

layout (location = 0) out vec4 diffuseOut;
layout (location = 1) out vec4 normalOut;
layout (location = 2) out float depthOut;

void main()
{
	vec4 red = vec4(1,0,0,1);

	diffuseOut = red;
	normalOut = Norm;
	depthOut = Depth;
}
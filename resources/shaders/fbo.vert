#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

uniform mat4 camera;

out vec2 UV;
out vec4 Norm;
out float Depth;


void main()
{
	vec4 p = camera * vec4(VertexPosition,1.0);
	
	gl_Position = p;
	
	UV = Texcoord;
	Norm = vec4(Normals, 1.0);
	Depth = p.z;
}
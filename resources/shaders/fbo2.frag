#version 330 core

in vec2 UV;

uniform sampler2D diff;
uniform sampler2D norm;
uniform sampler2D depth;

out vec4 fragColor;

void main()
{
	vec4 currentcol1 = texture(diff, vec2(UV.x, UV.y));
	fragColor = currentcol1;
}
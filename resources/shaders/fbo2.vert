#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

out vec2 UV;

void main()
{
	gl_Position = vec4(VertexPosition.xy, 0.0, 1.0);
	
	UV = Texcoord;
}
#version 330 core

layout (location=0) out vec4 FragColor;

uniform vec4 lightpos;

in vec3 normal;
in vec4 eye;

void main()
{
	vec4 diffuse = vec4(1,0,0,1);
	vec4 specular = vec4(1);
	// set the specular term to black
    vec4 spec = vec4(0.0);
 
    // normalize both input vectors
    vec3 n = normalize(normal);
    vec3 e = normalize(vec3(eye));
 
    float intensity = max(dot(n,lightpos.xyz), 0.0);
 
    // if the vertex is lit compute the specular color
    if (intensity > 0.0) {
        // compute the half vector
        vec3 h = normalize(lightpos.xyz + e); 
        // compute the specular term into spec
        float intSpec = max(dot(h,n), 0.0);
        spec = specular * pow(intSpec, 5.0);
    }
    FragColor = max(intensity *  diffuse + spec, diffuse);
}
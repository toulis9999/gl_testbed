#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

uniform mat4 MVP;
uniform mat4 MV;

out vec3 normal;
out vec4 eye;

void main()
{
	normal = normalize(Normals);
    eye = -(MV * vec4(VertexPosition,1.0));
    gl_Position = MVP * vec4(VertexPosition,1.0);
}
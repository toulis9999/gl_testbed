#version 330 core

in vec2 UV;
in vec3 Normals;

layout (location=0) out vec4 FragColor;

uniform sampler2D texture;
uniform sampler2D coor;
uniform float time;

void main()
{
    //1st we sample the UV distortion texture to get the coords to get the actual UV on the texture
    //we want to present for that pixel
    vec4 u = texture2D(coor, vec2(UV.x, UV.y));
    //using the previous UV's we sample from the main texture and we offsett by time
    vec4 pixel = texture2D(texture, vec2(u.x + time, u.y));
    //output the sampled fragment
    FragColor = pixel * u.a;
}

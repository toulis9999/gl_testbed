#version 330 core

layout (location=0) in vec3 VertexPosition;
layout (location=1) in vec2 Texcoord;
layout (location=2) in vec3 Normals;

uniform mat4 MVP;
uniform mat4 MV;
uniform vec4 lightpos;

out vec4 col;

void main()
{
	vec4 diffuse = vec4(1,0,0,1);
	// set the specular term initially to black
    vec4 spec = vec4(0.0);
 
    vec3 n = normalize(Normals);
 
    float intensity = max(dot(n, lightpos.xyz), 0.0);
 
    // if the vertex is lit compute the specular term
    if (intensity > 0.0) {
        // compute position in camera space
        vec3 pos = vec3(MV * vec4(VertexPosition,1.0));
        // compute eye vector and normalize it
        vec3 eye = normalize(-pos);
        // compute the half vector
        vec3 h = normalize(lightpos.xyz + eye);
 
        // compute the specular term into spec
        float intSpec = max(dot(h,n), 0.0);
        spec = vec4(1.0) * pow(intSpec, 5.0);
    }
    // add the specular term
    col = max(intensity *  diffuse + spec, diffuse);
 
    gl_Position = MVP * vec4(VertexPosition,1.0);
	
}
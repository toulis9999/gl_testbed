#include "include/application.h"
#include <iostream>

Application::Application(): running(true),
	mp_window(std::unique_ptr<SDL_Window, void (*)(SDL_Window*)>(nullptr, SDL_DestroyWindow))
{
	if (!initialise())
	{
		std::cout << "Failed to initialise" <<  std::endl;
		//return;
	}
	currentScene = 0;
}

Application::~Application()
{
	SDL_Quit();
}

void Application::insertScene(Scene* sc)
{
	sc->settings = &settings;
	sc->init();
	states.emplace_back(sc);
}

void Application::run()
{
	unsigned long long currenttime = SDL_GetPerformanceCounter();
	unsigned long long accumulator = 0;


	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	while (running)
	{
		newtime = SDL_GetPerformanceCounter();
		frametime = newtime - currenttime;
		currenttime = newtime;

		accumulator += frametime;


		while (accumulator >= dt)
		{
			//Fixed Update subloop
			while (SDL_PollEvent(&event))
			{
				//If user closes the window
				if (event.type == SDL_QUIT)
				{
					running = false;
				}
				if (!states.empty() && currentScene < states.size())
				{
					states[currentScene]->handleEvents(event);
				}

			}
			if (!states.empty() && currentScene < states.size())
			{
				states[currentScene]-> updateFixed(dt);
			}
			SDL_WarpMouseInWindow(NULL ,settings.getWindowWidth()/2, settings.getWindowHeight()/2);

			accumulator -= dt;
			t += dt;
		}
		//Variable update + draw subloop
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

		if (!states.empty() && currentScene < states.size())
		{
			states[currentScene]-> update(dt);
			states[currentScene]-> draw(dt);
			SDL_GL_SwapWindow(mp_window.get());
		}

	}
}

bool Application::initialise()
{
	settings.loadFromFile("../../resources/config.ini");

	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	mp_window.reset(SDL_CreateWindow(settings.getWindowTitle().c_str(),
																	 SDL_WINDOWPOS_CENTERED,
																	 SDL_WINDOWPOS_CENTERED,
																	 settings.getWindowWidth(),
																	 settings.getWindowHeight(),
																	 SDL_WINDOW_OPENGL |SDL_WINDOW_SHOWN));

	m_glcontext = SDL_GL_CreateContext(mp_window.get());
	SDL_GL_SetSwapInterval(settings.getVsync());
	SDL_ShowCursor(0);

	gladLoadGL();


	return true;
}

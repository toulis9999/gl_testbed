#include "include/camera.h"

#include <cmath>
#include "extlibs/fr_core/maths/fr_math.h"

static const float MaxVerticalAngle = 85.0f; //must be less than 90 to avoid gimbal lock

Camera::Camera(): m_position(0.0f, 0.0f, 1.0f),
	m_horizontalAngle(0.0f),
	m_verticalAngle(0.0f),
	m_fieldOfView(50.0f),
	m_nearPlane(0.01f),
	m_farPlane(100.0f),
	m_viewportAspectRatio(4.0f/3.0f)
{
}

const vec3f& Camera::position() const
{
	return m_position;
}

void Camera::setPosition(const vec3f& position)
{
	m_position = position;
}

void Camera::offsetPosition(const vec3f& offset)
{
	m_position += offset;
}

float Camera::fieldOfView() const
{
	return m_fieldOfView;
}

void Camera::setFieldOfView(float fieldOfView)
{
	assert(fieldOfView > 0.0f && fieldOfView < 180.0f);
	m_fieldOfView = fieldOfView;
}

float Camera::nearPlane() const
{
	return m_nearPlane;
}

float Camera::farPlane() const
{
	return m_farPlane;
}

void Camera::setNearAndFarPlanes(float nearPlane, float farPlane)
{
	assert(nearPlane > 0.0f);
	assert(farPlane > nearPlane);
	m_nearPlane = nearPlane;
	m_farPlane = farPlane;
}

mat4f Camera::orientation() const
{
	mat4f orientation; orientation.setIdentity();
	orientation *= rotate(toRadians(m_verticalAngle), vec3f(1,0,0));
	orientation *= rotate(toRadians(m_horizontalAngle), vec3f(0,1,0));
	return orientation;
}

void Camera::offsetOrientation(float upAngle, float rightAngle)
{
	m_horizontalAngle += rightAngle;
	m_verticalAngle += upAngle;
	normalizeAngles();
}

void Camera::lookAt(vec3f position)
{
	assert(position != m_position);
	vec3f direction = normalise(position - m_position);
	m_verticalAngle = toRadians(asinf(-direction.y));
	m_horizontalAngle = -toRadians(atan2f(-direction.x, -direction.z));
	normalizeAngles();
}

float Camera::viewportAspectRatio() const
{
	return m_viewportAspectRatio;
}

void Camera::setViewportAspectRatio(float viewportAspectRatio)
{
	assert(viewportAspectRatio > 0.0);
	m_viewportAspectRatio = viewportAspectRatio;
}

vec3f Camera::forward() const
{
	vec4f forward = orientation().inverse() * vec4f(0,0,-1,1);
	return vec3f(forward.x, forward.y, forward.z);
}

vec3f Camera::right() const
{
	vec4f right = orientation().inverse() * vec4f(1,0,0,1);
	return vec3f(right.x, right.y, right.z);
}

vec3f Camera::up() const
{
	vec4f up = orientation().inverse() * vec4f(0,1,0,1);
	return vec3f(up.x, up.y, up.z);
}

mat4f Camera::matrix() const
{
	return projection() * view();
}

mat4f Camera::projection() const
{
	return perspective(toRadians(m_fieldOfView), m_viewportAspectRatio, m_nearPlane, m_farPlane);
}

mat4f Camera::view() const
{
	return orientation() * translate(-m_position.x, -m_position.y, -m_position.z);
}

void Camera::normalizeAngles()
{
	m_horizontalAngle = fmodf(m_horizontalAngle, 360.0f);
	//fmodf can return negative values, but this will make them all positive
	if(m_horizontalAngle < 0.0f)
		m_horizontalAngle += 360.0f;
	if(m_verticalAngle > MaxVerticalAngle)
		m_verticalAngle = MaxVerticalAngle;
	else if(m_verticalAngle < -MaxVerticalAngle)
		m_verticalAngle = -MaxVerticalAngle;
}

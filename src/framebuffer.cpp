#include "include/framebuffer.h"
#include <iostream>


Framebuffer::Framebuffer(): bound(false), handle(0)
{
	glGetIntegerv(GL_VIEWPORT, OrigViewDims.data());
	glGenFramebuffers(1, &handle);
}

Framebuffer::~Framebuffer()
{
	if (bound) unbind();
	glViewport(OrigViewDims[0], OrigViewDims[1], OrigViewDims[2], OrigViewDims[3]);
	glDeleteFramebuffers(1, &handle);
}

void Framebuffer::attachTexture(const Texture& tex, GLenum attachPoint)
{
	if (!bound)
	{
		std::cout << "Framebuffer not bound! Cannot attach texture" << std::endl;
		return;
	}
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachPoint, GL_TEXTURE_2D, tex.getHandle(), 0);

	if (attachPoint != GL_DEPTH_ATTACHMENT)
	{
		width = tex.getWidth();
		height = tex.getHeight();
		attachments.push_back(attachPoint);
	}
}

void Framebuffer::clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Framebuffer::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, handle);
	glDrawBuffers(attachments.size(), attachments.data());
	glViewport(0, 0, width, height);
	bound = true;
}

void Framebuffer::unbind()
{
	if (bound)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(OrigViewDims[0], OrigViewDims[1], OrigViewDims[2], OrigViewDims[3]);
		bound = false;
	}
}

bool Framebuffer::isBound() const
{
	return bound;
}

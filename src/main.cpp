#include <memory>
#include <string>
#include <iostream>

#include <SDL.h>

#include "extlibs/fr_core/maths/fr_math.h"

#include "include/settings.h"
#include "include/texture.h"
#include "include/shader.h"
#include "include/mesh.h"
#include "include/camera.h"

#include "include/application.h"
#include "include/scene.h"

#include "test_scenes/fboscene.h"
#include "test_scenes/mipmapscene.h"
#include "test_scenes/lightscene.h"
#include "test_scenes/distscene.h"

int main(int argc, char *argv[])
{
	Application app;
	app.insertScene(new DistScene);
	app.run();
	return 0;
}

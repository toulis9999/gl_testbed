#include "include/mesh.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>

#include <regex> // for the parsing


#include "extlibs/fr_core/maths/fr_math.h"
#include "include/util.h"

Mesh::Mesh(): vbo_pos_handle(0),
	vbo_uv_handle(0),
	vbo_normals_handle(0),
	has_uvs(false),
	has_normals(false)
{}

Mesh::~Mesh() { glDeleteVertexArrays(1, &vao_handle); }

void Mesh::loadFromFile(const std::string& fileName)
{
	//check if file exists
	if (!util::fileExists(fileName))
	{
		std::cerr << "The file " << fileName << " Does not exist" << std::endl;
		return;
	}
	std::vector<float> finpositions;
	std::vector<float> finuvs;
	std::vector<float> finnormals;
	
	std::vector<unsigned int> faceorder;
	std::vector<unsigned int> uvorder;
	std::vector<unsigned int> normalorder;

	std::vector<float> positions;
	std::vector<float> normals;
	std::vector<float> texCoords;

	std::ifstream filestream;
	filestream.open(fileName.c_str());

	std::string line_stream;


	//Optimisation prepass of the file to find number of elements
	unsigned int verts_num = 0, uvs_num = 0, normals_num = 0;
	while (std::getline(filestream, line_stream))
	{
		std::stringstream str_stream(line_stream);
		std::string type_str;
		str_stream >> type_str;
		if (type_str == "v")
		{
			++verts_num;
		}
		if (type_str == "vt")
		{
			++uvs_num;
		}
		if (type_str == "vn")
		{
			++normals_num;
		}
	}
	if (uvs_num) has_uvs = true;
	if (normals_num) has_normals = true;
	//reserve the vector sizes to avoid multi-allocations
	positions.reserve(verts_num * 3);
	texCoords.reserve(uvs_num * 2);
	normals.reserve(normals_num * 3);

	//each vertex uv and normal can be shared by 2 or more faces so we reserve accordingly
	faceorder.reserve(verts_num * 3 * 2);
	uvorder.reserve(uvs_num * 2 * 2);
	normalorder.reserve(normals_num * 3 * 2);

	//since we duplicate each face element if it reappears we reserve accordingly
	finpositions.reserve(verts_num * 3 * 2 * 3);
	finuvs.reserve(uvs_num * 2 * 2 * 3);
	finnormals.reserve(normals_num * 3 * 2 * 3);


	//return the stream to the beginning of the file
	filestream.clear();
	filestream.seekg(0, std::ios::beg);


	while (std::getline(filestream, line_stream))
	{
		std::stringstream str_stream(line_stream);
		std::string type_str;
		str_stream >> type_str;
		if (type_str == "v")
		{
			vec3f pos;
			str_stream >> pos.x >> pos.y >> pos.z;
			positions.push_back(pos.x);
			positions.push_back(pos.y);
			positions.push_back(pos.z);
		}
		else if (type_str == "vt")
		{
			vec2f tex;
			str_stream >> tex.x >> tex.y;
			texCoords.push_back(tex.x);
			texCoords.push_back(tex.y);
		}
		else if (type_str == "vn")
		{
			vec3f nor;
			str_stream >> nor.x >> nor.y >> nor.z;
			normals.push_back(nor.x);
			normals.push_back(nor.y);
			normals.push_back(nor.z);
		}
		else if (type_str == "f")
		{
			char interupt;
			for (int i = 0; i < 3; ++i)
			{
				int first, sec, third;

				str_stream >> first;
				faceorder.push_back(first);
				if (has_uvs && has_normals)
				{
					str_stream >> interupt >> sec >> interupt >> third;
					uvorder.push_back(sec);
					normalorder.push_back(third);
					continue;
				}
				if (has_uvs)
				{
					str_stream >> interupt >> sec;
					uvorder.push_back(sec);
				}
				if (has_normals)
				{
					str_stream >> interupt >> interupt >> third;
					normalorder.push_back(third);
				}
			}
		}
	}

	for (const auto& i : faceorder)
	{
		unsigned int v = (i - 1) * 3;
		finpositions.push_back(positions[v]);
		finpositions.push_back(positions[v + 1]);
		finpositions.push_back(positions[v + 2]);
	}

	for (const auto& i : uvorder)
	{
		unsigned int v = (i - 1) * 2;
		finuvs.push_back(texCoords[v]);
		finuvs.push_back(texCoords[v + 1]);
	}

	for (const auto& i : normalorder)
	{
		unsigned int v = (i - 1) * 3;
		finnormals.push_back(normals[v]);
		finnormals.push_back(normals[v + 1]);
		finnormals.push_back(normals[v + 2]);
	}

	filestream.close();

	// Create and populate the buffer objects
	glGenBuffers(1, &vbo_pos_handle);
	if (has_uvs) glGenBuffers(1, &vbo_uv_handle);
	if (has_normals) glGenBuffers(1, &vbo_normals_handle);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_pos_handle);
	glBufferData(GL_ARRAY_BUFFER, finpositions.size() * sizeof(float), &finpositions[0], GL_STATIC_DRAW);

	if (has_uvs)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_uv_handle);
		glBufferData(GL_ARRAY_BUFFER, finuvs.size() * sizeof(float), &finuvs[0], GL_STATIC_DRAW);
	}

	if (has_normals)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_normals_handle);
		glBufferData(GL_ARRAY_BUFFER, finnormals.size() * sizeof(float), &finnormals[0], GL_STATIC_DRAW);
	}
	// Create and set-up the vertex array object
	glGenVertexArrays( 1, &vao_handle);
	glBindVertexArray(vao_handle);

	glEnableVertexAttribArray(0); // Vertex position
	if (has_uvs) glEnableVertexAttribArray(1); // Vertex texcoords
	if (has_normals) glEnableVertexAttribArray(2); // Vertex normals

	glBindBuffer(GL_ARRAY_BUFFER, vbo_pos_handle);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );

	if (has_uvs)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_uv_handle);
		glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	}

	if (has_normals)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_normals_handle);
		glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	}

	glBindVertexArray(0);
	num_tris = finpositions.size() /3;
}

void Mesh::bind()
{
	glBindVertexArray(vao_handle);
	bound = true;
}

void Mesh::unbind()
{
	glBindVertexArray(0);
	bound = false;
}

void Mesh::draw()
{
	glDrawArrays(GL_TRIANGLES, 0, num_tris);
}

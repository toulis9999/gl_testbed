#include "include/program.h"

#include "extlibs/glfuncs/glad.h"

Program::~Program() { glDeleteProgram(m_handle); }

bool Program::create(uint32_t vertex_shader_handle, uint32_t pixel_shader_handle)
{
	assert(!m_created); //already created
	m_handle = glCreateProgram();
	glAttachShader(m_handle, vertex_shader_handle);
	glAttachShader(m_handle, pixel_shader_handle);

	glLinkProgram(m_handle);

	GLint status;
	glGetProgramiv(m_handle, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) return false;

	glValidateProgram(m_handle);

	glGetProgramiv(m_handle, GL_VALIDATE_STATUS, &status);
	if (status == GL_FALSE) return false;

	m_created = true;
	m_uniformLocations.clear();
	return true;
}

void Program::bind()
{
	assert(m_handle);
	glUseProgram(m_handle);
	m_bound = true;
}

void Program::unbind() { glUseProgram(0); }

void Program::setUniform(const char* name, const vec2f& v)
{
	GLint loc = getUniformLocation(name);
	glUniform2f(loc, v.x, v.y);
}

void Program::setUniform(const char* name, const vec3f& v)
{
	GLint loc = getUniformLocation(name);
	glUniform3f(loc, v.x, v.y, v.z);
}

void Program::setUniform(const char* name, const vec4f& v)
{
	GLint loc = getUniformLocation(name);
	glUniform4f(loc, v.x, v.y, v.z, v.w);
}

void Program::setUniform(const char* name, const mat4f& m)
{
	GLint loc = getUniformLocation(name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, &m.m[0][0]);
}

void Program::setUniform(const char* name, const mat3f& m)
{
	GLint loc = getUniformLocation(name);
	glUniformMatrix3fv(loc, 1, GL_FALSE, &m.m[0][0]);
}

void Program::setUniform(const char* name, float val)
{
	GLint loc = getUniformLocation(name);
	glUniform1f(loc, val);
}

void Program::setUniform(const char* name, int val)
{
	GLint loc = getUniformLocation(name);
	glUniform1i(loc, val);
}

void Program::setUniform(const char* name, bool val)
{
	GLint loc = getUniformLocation(name);
	glUniform1ui(loc, val);
}

int Program::getUniformLocation(const char* name)
{
	//try to find the name in the map
	auto pos = m_uniformLocations.find(name);

	//if not found query openGL state and store it in the map
	if (pos == m_uniformLocations.end())
	{
		m_uniformLocations[name] = glGetUniformLocation(m_handle, name);
	}
	return m_uniformLocations[name];
}

int Program::getErrorLength()
{
	int length = 0;
	glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &length);
	return length;
}

void Program::getLastError(char* out)
{
	int written;
	glGetProgramInfoLog(m_handle, getErrorLength(), &written, out);
}

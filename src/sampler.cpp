#include "include/sampler.h"

#include <assert.h>
#include "extlibs/glfuncs/glad.h"

GLenum FilteringTypesGL[] =
{
	GL_NEAREST,
	GL_LINEAR,
	GL_NEAREST_MIPMAP_NEAREST,
	GL_LINEAR_MIPMAP_NEAREST,
	GL_NEAREST_MIPMAP_LINEAR,
	GL_LINEAR_MIPMAP_LINEAR
};

GLenum RepeatModesGL[] =
{
	GL_REPEAT,
	GL_MIRRORED_REPEAT,
	GL_CLAMP_TO_EDGE
};

Sampler::Sampler(): m_handle(0),
	m_minFilter(FilteringMode::NEAREST), m_magFilter(FilteringMode::NEAREST)
	, m_edgeWrapU(RepeatMode::CLAMP_EDGE), m_edgeWrapV(RepeatMode::CLAMP_EDGE), m_edgeWrapW(RepeatMode::CLAMP_EDGE)
{
	glGenSamplers(1, &m_handle);
	glSamplerParameteri(m_handle, GL_TEXTURE_MIN_FILTER, FilteringTypesGL[m_minFilter]);
	glSamplerParameteri(m_handle, GL_TEXTURE_MAG_FILTER, FilteringTypesGL[m_magFilter]);
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_S, RepeatModesGL[m_edgeWrapU]);
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_T, RepeatModesGL[m_edgeWrapV]);
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_R, RepeatModesGL[m_edgeWrapW]);
}

Sampler::~Sampler() { glDeleteSamplers(1, &m_handle); }

void Sampler::bind(uint32_t TextureUnit)
{
	if (m_handle == 0) return; // check if handle is valid
	glBindSampler(TextureUnit, m_handle);
}

void Sampler::unbind(uint32_t TextureUnit) { glBindSampler(TextureUnit, 0); }

void Sampler::setMinFilter(const FilteringMode& value)
{
	glSamplerParameteri(m_handle, GL_TEXTURE_MIN_FILTER, FilteringTypesGL[value]);
	m_minFilter = value;
}

void Sampler::setMagFilter(const FilteringMode& value)
{
	//no mipmap filtering on magnification!
	assert(value != FilteringMode::LINEAR_MIP_LINEAR
		&& value != FilteringMode::LINEAR_MIP_NEAREST
		&& value != FilteringMode::NEAREST_MIP_LINEAR
		&& value != FilteringMode::NEAREST_MIP_NEAREST);

	glSamplerParameteri(m_handle, GL_TEXTURE_MAG_FILTER, FilteringTypesGL[value]);
	m_magFilter = value;
}

void Sampler::setEdgeWrapU(const RepeatMode& value)
{
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_S, RepeatModesGL[value]);
	m_edgeWrapU = value;
}

void Sampler::setEdgeWrapV(const RepeatMode& value)
{
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_T, RepeatModesGL[value]);
	m_edgeWrapV = value;
}

void Sampler::setEdgeWrapW(const RepeatMode& value)
{
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_R, RepeatModesGL[value]);
	m_edgeWrapW = value;
}

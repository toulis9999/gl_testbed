#include "include/settings.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include "include/util.h"


Settings::Settings():win_height(600),win_width(800), window_title(""),vsync(true)
{
}

void Settings::loadFromFile(const std::string& fileName)
{
	//check if file exists
	if (!util::fileExists(fileName))
	{
		std::cerr << "The file " << fileName << " Does not exist" << std::endl;
		return;
	}
	std::ifstream filestream;
	filestream.open(fileName.c_str());

	std::string line_stream;

	while (std::getline(filestream, line_stream))
	{
		std::stringstream str_stream(line_stream);
		std::string type_str;
		str_stream >> type_str;
		if (type_str == "width")
		{
			int width;
			char eq;
			str_stream >> eq >> width;
			win_width = width;
		}
		if (type_str == "height")
		{
			int height;
			char eq;
			str_stream >> eq >> height;
			win_height = height;
		}
		if (type_str == "vsync")
		{
			bool sync;
			char eq;
			str_stream >> eq >> sync;
			vsync = sync;
		}
		if (type_str == "title")
		{
			std::string titl;
			char eq;
			str_stream >> eq;
			std::getline(str_stream, titl);
			//trim white space back and front
			titl.erase(titl.begin(), std::find_if(titl.begin(), titl.end(), std::bind1st(std::not_equal_to<char>(), ' ')));
			titl.erase(std::find_if(titl.rbegin(), titl.rend(), std::bind1st(std::not_equal_to<char>(), ' ')).base(), titl.end());
			window_title = titl;
		}
		if (type_str == "res")
		{
			std::string res;
			char eq;
			str_stream >> eq;
			std::getline(str_stream, res);
			//trim white space back and front
			res.erase(res.begin(), std::find_if(res.begin(), res.end(), std::bind1st(std::not_equal_to<char>(), ' ')));
			res.erase(std::find_if(res.rbegin(), res.rend(), std::bind1st(std::not_equal_to<char>(), ' ')).base(), res.end());
			resources_folder = res;
		}
	}
	ratio = static_cast<float>(win_width) / static_cast<float>(win_height);
}
int Settings::getWindowHeight() const
{
	return win_height;
}

int Settings::getWindowWidth() const
{
	return win_width;
}

float Settings::getAspect() const
{
	return ratio;
}

bool Settings::getVsync() const
{
	return vsync;
}

std::string Settings::getWindowTitle() const
{
	return window_title;
}

std::string Settings::getResourcesFolder() const
{
	return resources_folder;
}




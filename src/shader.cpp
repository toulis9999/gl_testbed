#include "include/shader.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "extlibs/glfuncs/glad.h"

static GLenum ShaderTypeGL[] =
{
	GL_VERTEX_SHADER,
	GL_FRAGMENT_SHADER
};

Shader::~Shader() { glDeleteShader(m_handle); }

bool Shader::compileShader(const char* fileName, ShaderType t)
{
	assert(!m_handle); //shader already compiled
	assert(fileName); //empty filename
	FILE *f = fopen(fileName, "rb");
	assert(f); //could not open file
	fseek(f, 0, SEEK_END);
	size_t fsize = static_cast<size_t>(ftell(f));
	rewind(f);
	auto buf = static_cast<char*>(malloc(fsize + 1));
	assert(buf); //failed allocation
	fread(buf, fsize, 1, f);
	fclose(f);
	buf[fsize] = 0;

	m_handle = glCreateShader(ShaderTypeGL[static_cast<size_t>(t)]);
	m_type = t;

	glShaderSource(m_handle, 1, &buf, nullptr);
	glCompileShader(m_handle);

	free(buf);

	//check for compilation errors
	int result;
	glGetShaderiv(m_handle, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) return false;
	return true;
}

int Shader::getErrorLength()
{
	int length = 0;
	glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &length);
	return length;
}

void Shader::getLastError(char* out)
{
	int written;
	glGetShaderInfoLog(m_handle, getErrorLength(), &written, out);
}

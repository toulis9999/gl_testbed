#include "include/texture.h"

#include <algorithm>
#include <cmath>

#include "extlibs/glfuncs/glad.h"

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif

#include "extlibs/stb/stb_image.h"
#include "extlibs/fr_core/maths/fr_math.h"

static const GLenum cubeEnums[] = { GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z };

static const GLenum TexTypesGL[] =
{
	GL_TEXTURE_1D,
	GL_TEXTURE_2D,
	GL_TEXTURE_3D,
	GL_TEXTURE_CUBE_MAP
};

static const GLenum TexFormatsGL[] =
{
	GL_RGBA
};

int getCurrentActiveUnit()
{
	int n;
	glGetIntegerv(GL_ACTIVE_TEXTURE, &n);
	return n;
}

Texture::~Texture()
{
	if (m_bound) unbind();
	glDeleteTextures(1, &m_handle);
}

void Texture::createFromFile2D(const char* fileName)
{
	assert(fileName && !m_handle);
	int n;
	unsigned char *data = stbi_load(fileName, &m_width, &m_height, &n, 4); //Currently all textures are loaded as RGBA for convenience
	assert(data);

	glGenTextures(1, &m_handle);
	m_type = TextureType::TEXTURE_2D;

	int prevunit = getCurrentActiveUnit();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(TexTypesGL[m_type], m_handle);
	glTexImage2D(TexTypesGL[m_type], 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(TexTypesGL[m_type], 0);
	glActiveTexture(static_cast<GLenum>(prevunit));

	stbi_image_free(data);
}

void Texture::createFromFilesCube(const char* fName1, const char* fName2, const char* fName3, const char* fName4, const char* fName5, const char* fName6)
{
	assert(fName1 && fName2 && fName3 && fName4 && fName5 && fName6);

	const char* names[] = { fName1, fName2, fName3, fName4, fName5, fName6 };
	unsigned char *data[6]; vec2i dims[6];

	for (int i = 0; i < 6; ++i)
	{
		int n;
		data[i] = stbi_load(names[i], &dims[i].x, &dims[i].y, &n, 4); //Currently all textures are loaded as RGBA for convenience
		if (!data[i])
		{
			for (int j = 0; j < 6; ++j) stbi_image_free(data[j]);
			return;
		}
	}
	//check if loaded textures are the same size
	for (int i = 1; i < 6; ++i)
	{
		if (dims[i] != dims[i-1])
		{
			for (int j = 0; j < 6; ++j) stbi_image_free(data[j]);
			return;
		}
	}
	assert(!m_handle);

	glGenTextures(1, &m_handle);
	m_type = TextureType::TEXTURE_CUBE;
	m_width = dims[0].x; m_height = dims[0].y;

	int prevunit = getCurrentActiveUnit();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(TexTypesGL[m_type], m_handle);
	for (int i = 0; i < 6; ++i)
	{
		glTexImage2D(cubeEnums[i], 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	glBindTexture(TexTypesGL[m_type], 0);
	glActiveTexture(static_cast<GLenum>(prevunit));

	for (int i = 0; i < 6; ++i) stbi_image_free(data[i]);
}

void Texture::createEmpty2D(int w, int h, bool depth)
{
	assert(!m_handle);
	m_width = w;
	m_height = h;

	glGenTextures(1, &m_handle);
	m_type = TextureType::TEXTURE_2D;

	int prevunit = getCurrentActiveUnit();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(TexTypesGL[m_type], m_handle);
	glTexImage2D(TexTypesGL[m_type], 0, (depth ? GL_DEPTH_COMPONENT : GL_RGBA), m_width, m_height, 0, (depth ? GL_DEPTH_COMPONENT : GL_RGBA), GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(TexTypesGL[m_type], 0);
	glActiveTexture(static_cast<GLenum>(prevunit));
}

void Texture::createEmptyCube(int w, int h)
{
	assert(!m_handle);
	glGenTextures(1, &m_handle);
	m_type = TextureType::TEXTURE_CUBE;

	m_width = w;
	m_height = h;

	int prevunit = getCurrentActiveUnit();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(TexTypesGL[m_type], m_handle);
	for (int i = 0; i < 6; ++i)
	{
		glTexImage2D(cubeEnums[i], 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	glBindTexture(TexTypesGL[m_type], 0);
	glActiveTexture(static_cast<GLenum>(prevunit));
}

void Texture::generateMipChain()
{
	if (m_mipmapLevels) return;
	bind(0);
	glGenerateMipmap(TexTypesGL[m_type]);
	//set trilinnear filtering when MipMaps are present
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	unbind();
	m_mipmapLevels = static_cast<int>(1 + std::floor(std::log2(std::max(m_width, m_height))));
}

void Texture::bind(int Activeunit)
{
	assert(m_handle && !m_bound);
	glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + Activeunit));
	glBindTexture(TexTypesGL[m_type], m_handle);
	m_texUnit = Activeunit;
	m_bound = true;
}

void Texture::unbind()
{
	assert(m_bound);
	glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + m_texUnit));
	glBindTexture(TexTypesGL[m_type], 0);
	m_bound = false;
}

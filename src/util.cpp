#include "include/util.h"

#include <vector>
#include <fstream>

#include "extlibs/glfuncs/glad.h"

#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#endif

#include "extlibs/stb/stb_image_write.h"

namespace util
{
	bool fileExists(const std::string& name)
	{
		std::ifstream f(name.c_str());
		bool ret = f.good();
		f.close();
		return ret;
	}

	bool screenGrab(int w, int h, const char* filename, bool flipY)
	{
		glPixelStorei(GL_PACK_ALIGNMENT, 1);

		std::vector<char> dataBuffer(w * h * 3);

		glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, dataBuffer.data());

		if (flipY)
		{
			std::vector<char> flipBuffer(w * 3);
			for (auto i = 0; i < h/2 ; ++i)
			{
				auto n  = i * w * 3;
				auto nn = (h-i-1) * w * 3;
				char* src = &dataBuffer[n];
				char* dest = &dataBuffer[nn];
				memcpy(flipBuffer.data(), dest, w * 3);
				memcpy(dest, src, w * 3);
				memcpy(src, flipBuffer.data(), w * 3);
			}
		}

		auto ret = stbi_write_png(filename, w, h, 3, dataBuffer.data(), w*3);
		return ret;
	}
}

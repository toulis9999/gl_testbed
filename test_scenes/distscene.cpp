#include "distscene.h"

#include <iostream>
#include "include/util.h"

DistScene::DistScene()
{
}

void DistScene::init()
{
	body.loadFromFile(settings->getResourcesFolder() + "models/body.obj");
	head.loadFromFile(settings->getResourcesFolder() + "models/head.obj");
	plane.loadFromFile(settings->getResourcesFolder() + "models/plane.obj");

	cam.setPosition(vec3f(0,0,4));
	cam.setViewportAspectRatio(settings->getAspect());

	auto str = settings->getResourcesFolder() + "textures/body_d.png";
	body_d.createFromFile2D(str.c_str());
	body_d.generateMipChain();
	str = settings->getResourcesFolder() + "textures/head_d.png";
	head_d.createFromFile2D(str.c_str());
	head_d.generateMipChain();

	str = settings->getResourcesFolder() + "textures/distort.png";
	distort_tex.createFromFile2D(str.c_str());
	distort_tex.generateMipChain();
	str = settings->getResourcesFolder() + "textures/map.png";
	map_tex.createFromFile2D(str.c_str());
	map_tex.generateMipChain();

	linearSampler.setEdgeWrapU(RepeatMode::REPEAT);
	linearSampler.setEdgeWrapU(RepeatMode::REPEAT);
	linearSampler.setMinFilter(FilteringMode::LINEAR_MIP_LINEAR);
	linearSampler.setMagFilter(FilteringMode::LINEAR);

	auto str1 = settings->getResourcesFolder() + "shaders/basic.vert";
	auto str2 = settings->getResourcesFolder() + "shaders/basic.frag";
	auto str3 = settings->getResourcesFolder() + "shaders/uv_distort.frag";

	basicvs.compileShader(str1.c_str(), ShaderType::VERTEX);
	basicfs.compileShader(str2.c_str(), ShaderType::FRAGMENT);
	distortfs.compileShader(str3.c_str(), ShaderType::FRAGMENT);

	texprog.create(basicvs.getHandle(), basicfs.getHandle());

	distortprog.create(basicvs.getHandle(), distortfs.getHandle());

	elapsedTime = 0;

	linearSampler.bind(0);
	linearSampler.bind(1);
	linearSampler.bind(2);
	linearSampler.bind(3);
	body_d.bind(0);
	head_d.bind(1);
	map_tex.bind(2);
	distort_tex.bind(3);
}

void DistScene::handleEvents(const SDL_Event& event)
{
	//If user moves the mouse
	if (event.type == SDL_MOUSEMOTION)
	{
		int x = event.motion.x - settings->getWindowWidth()/2;
		int y = event.motion.y - settings->getWindowHeight()/2;
		cam.offsetOrientation(0.05f * (float)y, 0.05f * (float)x);
	}

}

void DistScene::updateFixed(unsigned long long dt)
{
	SDL_PumpEvents();
	int statelen;
	const Uint8 *state = SDL_GetKeyboardState(&statelen);

	//If user presses any key
	if(state[SDL_SCANCODE_S]) cam.offsetPosition(0.05f * -cam.forward());
	if(state[SDL_SCANCODE_W]) cam.offsetPosition(0.05f * cam.forward());
	if(state[SDL_SCANCODE_A]) cam.offsetPosition(0.05f * -cam.right());
	if(state[SDL_SCANCODE_D]) cam.offsetPosition(0.05f * cam.right());
	if(state[SDL_SCANCODE_Z]) cam.offsetPosition(0.05f * -vec3f(0,1,0));
	if(state[SDL_SCANCODE_X]) cam.offsetPosition(0.05f * vec3f(0, 1, 0));

	if (state[SDL_SCANCODE_SPACE])
	{
		std::string name = "ScreenShot_.png";
		util::screenGrab(settings->getWindowWidth(), settings->getWindowHeight(), name.c_str(), true);
	}


	elapsedTime += static_cast<float>(dt) / 5000000.f;
}

void DistScene::update(unsigned long long dt)
{

}

void DistScene::draw(unsigned long long dt)
{
	texprog.bind();
		texprog.setUniform("camera", cam.matrix());
		texprog.setUniform("tex", body_d.getUnit());
		body.bind();
			body.draw();
		body.unbind();
		texprog.setUniform("tex", head_d.getUnit());
		head.bind();
			head.draw();
		head.unbind();
	texprog.unbind();

	distortprog.bind();
		distortprog.setUniform("camera", cam.matrix());
		distortprog.setUniform("texture", map_tex.getUnit());
		distortprog.setUniform("coor", distort_tex.getUnit());
		distortprog.setUniform("time", elapsedTime);
		plane.bind();
			plane.draw();
		plane.unbind();
	distortprog.unbind();

}

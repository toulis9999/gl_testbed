#ifndef DISTSCENE_H
#define DISTSCENE_H

#include "../include/scene.h"

#include "../include/mesh.h"
#include "../include/camera.h"
#include "../include/Texture.h"
#include "../include/shader.h"
#include "../include/program.h"
#include "../include/sampler.h"


class DistScene: public Scene
{
public:
	DistScene();

	virtual void init() override;
	virtual void handleEvents(const SDL_Event& event) override;
	virtual void updateFixed(unsigned long long dt) override;
	virtual void update(unsigned long long dt) override;
	virtual void draw(unsigned long long dt) override;

private:
	Mesh body, head, plane;
	Camera cam;
	Texture body_d, head_d, distort_tex, map_tex;
	Sampler linearSampler;
	Shader basicvs,basicfs, distortfs;
	Program texprog, distortprog;

	float elapsedTime;
};

#endif // SCENE1_H

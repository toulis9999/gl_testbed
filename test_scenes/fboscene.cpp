#include "fboscene.h"

#include <iostream>

FboScene::FboScene()
{
}

void FboScene::init()
{
	diffuse.createEmpty2D(settings->getWindowWidth()/4, settings->getWindowHeight()/4);
	normals.createEmpty2D(settings->getWindowWidth()/4, settings->getWindowHeight()/4);
	depth.createEmpty2D(settings->getWindowWidth(), settings->getWindowHeight(),true);

	fbo.bind();
	fbo.attachTexture(diffuse, GL_COLOR_ATTACHMENT0);
	fbo.attachTexture(normals, GL_COLOR_ATTACHMENT1);
	fbo.attachTexture(depth, GL_DEPTH_ATTACHMENT);
	fbo.unbind();

	ground.loadFromFile(settings->getResourcesFolder() + "models/ground.obj");
	torus.loadFromFile(settings->getResourcesFolder() + "models/torus.obj");
	screenquad.loadFromFile(settings->getResourcesFolder() + "models/screenquad.obj");

	cam.setPosition(vec3f(0,0,4));
	cam.setViewportAspectRatio(settings->getAspect());

	auto str1 = settings->getResourcesFolder() + "shaders/fbo.vert";
	auto str2 = settings->getResourcesFolder() + "shaders/fbo.frag";
	fboVert.compileShader(str1.c_str(), ShaderType::VERTEX);
	fboFrag.compileShader(str2.c_str(), ShaderType::FRAGMENT);

	fboPass.create(fboVert.getHandle(), fboFrag.getHandle());

	str1 = settings->getResourcesFolder() + "shaders/fbo2.vert";
	str2 = settings->getResourcesFolder() + "shaders/fbo2.frag";
	quadVert.compileShader(str1.c_str(), ShaderType::VERTEX);
	quadFrag.compileShader(str2.c_str(), ShaderType::FRAGMENT);

	quadPass.create(quadVert.getHandle(), quadFrag.getHandle());

	elapsedTime = 0;

	diffuse.bind(0);
	normals.bind(1);
	depth.bind(2);
}

void FboScene::handleEvents(const SDL_Event& event)
{
	//If user moves the mouse
	if (event.type == SDL_MOUSEMOTION)
	{
		int x = event.motion.x - settings->getWindowWidth()/2;
		int y = event.motion.y - settings->getWindowHeight()/2;
		cam.offsetOrientation(0.05f * (float)y, 0.05f * (float)x);
	}

}

void FboScene::updateFixed(unsigned long long dt)
{
	SDL_PumpEvents();
	int statelen;
	const Uint8 *state = SDL_GetKeyboardState(&statelen);

	//If user presses any key
	if (state[SDL_SCANCODE_S]) cam.offsetPosition(0.05f * -cam.forward());
	if (state[SDL_SCANCODE_W]) cam.offsetPosition(0.05f * cam.forward());
	if (state[SDL_SCANCODE_A]) cam.offsetPosition(0.05f * -cam.right());
	if (state[SDL_SCANCODE_D]) cam.offsetPosition(0.05f * cam.right());
	if (state[SDL_SCANCODE_Z]) cam.offsetPosition(0.05f * -vec3f(0, 1, 0));
	if (state[SDL_SCANCODE_X]) cam.offsetPosition(0.05f * vec3f(0, 1, 0));

	elapsedTime += static_cast<float>(dt) / 5000000.f;
}

void FboScene::update(unsigned long long dt)
{

}

void FboScene::draw(unsigned long long dt)
{
	fbo.bind();
	fbo.clear();
		fboPass.bind();
		fboPass.setUniform("camera", cam.matrix());
			torus.bind();
				torus.draw();
			torus.unbind();
		fboPass.unbind();
	fbo.unbind();

	quadPass.setUniform("diff",diffuse.getUnit());
	quadPass.setUniform("norm",normals.getUnit());
	quadPass.setUniform("depth",depth.getUnit());
	quadPass.bind();
		screenquad.bind();
			screenquad.draw();
		screenquad.unbind();
	quadPass.unbind();
}

#ifndef FBOSCENE_H
#define FBOSCENE_H
#include "../include/scene.h"

#include "../include/mesh.h"
#include "../include/camera.h"
#include "../include/Texture.h"
#include "../include/shader.h"
#include "../include/program.h"
#include "../include/framebuffer.h"


class FboScene: public Scene
{
public:
	FboScene();

	virtual void init() override;
	virtual void handleEvents(const SDL_Event& event) override;
	virtual void updateFixed(unsigned long long dt) override;
	virtual void update(unsigned long long dt) override;
	virtual void draw(unsigned long long dt) override;

private:
	Framebuffer fbo;
	Mesh ground, torus, screenquad;
	Camera cam;

	Texture diffuse, normals, depth;
	Shader fboVert, fboFrag, quadVert, quadFrag;
	Program fboPass, quadPass;
	float elapsedTime;
};

#endif // SCENE1_H

#include "lightscene.h"

#include <iostream>

LightScene::LightScene()
{
}

void LightScene::init()
{
	ball.loadFromFile(settings->getResourcesFolder() + "models/monkey.obj");

	cam.setPosition(vec3f(0,0,4));
	cam.setViewportAspectRatio(settings->getAspect());

	auto str1 = settings->getResourcesFolder() + "shaders/plight.vert";
	auto str2 = settings->getResourcesFolder() + "shaders/plight.frag";

	vshad.compileShader(str1.c_str(), ShaderType::VERTEX);
	fshad.compileShader(str2.c_str(), ShaderType::FRAGMENT);

	dirlight.create(vshad.getHandle(), fshad.getHandle());

	lpos = vec4f(0,0.5,0.5,0);

	elapsedTime = 0;
}

void LightScene::handleEvents(const SDL_Event& event)
{
	//If user moves the mouse
	if (event.type == SDL_MOUSEMOTION)
	{
		int x = event.motion.x - settings->getWindowWidth()/2;
		int y = event.motion.y - settings->getWindowHeight()/2;
		cam.offsetOrientation(0.05f * (float)y, 0.05f * (float)x);
	}

}

void LightScene::updateFixed(unsigned long long dt)
{
	SDL_PumpEvents();
	int statelen;
	const Uint8 *state = SDL_GetKeyboardState(&statelen);

	//If user presses any key
	if (state[SDL_SCANCODE_S]) cam.offsetPosition(0.05f * -cam.forward());
	if (state[SDL_SCANCODE_W]) cam.offsetPosition(0.05f * cam.forward());
	if (state[SDL_SCANCODE_A]) cam.offsetPosition(0.05f * -cam.right());
	if (state[SDL_SCANCODE_D]) cam.offsetPosition(0.05f * cam.right());
	if (state[SDL_SCANCODE_Z]) cam.offsetPosition(0.05f * -vec3f(0, 1, 0));
	if (state[SDL_SCANCODE_X]) cam.offsetPosition(0.05f * vec3f(0, 1, 0));

	elapsedTime += static_cast<float>(dt) / 500000.f;
}

void LightScene::update(unsigned long long dt)
{

}

void LightScene::draw(unsigned long long dt)
{
	lpos.y = std::sin(elapsedTime);
	lpos.x = std::cos(elapsedTime);
	dirlight.bind();
	dirlight.setUniform("MVP", cam.matrix());
	dirlight.setUniform("MV", cam.view());
	dirlight.setUniform("lightpos", lpos);
	ball.bind();
	ball.draw();
	ball.unbind();
	dirlight.unbind();
}

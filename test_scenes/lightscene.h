#ifndef LIGHTSCENE_H
#define LIGHTSCENE_H

#include "../include/scene.h"

#include "../include/mesh.h"
#include "../include/camera.h"
#include "../include/Texture.h"
#include "../include/shader.h"
#include "../include/program.h"


class LightScene: public Scene
{
public:
	LightScene();

	virtual void init() override;
	virtual void handleEvents(const SDL_Event& event) override;
	virtual void updateFixed(unsigned long long dt) override;
	virtual void update(unsigned long long dt) override;
	virtual void draw(unsigned long long dt) override;

private:
	Mesh ball;
	Camera cam;
	Shader vshad, fshad;
	Program dirlight;

	float elapsedTime;
	vec4f lpos;
};

#endif // SCENE1_H

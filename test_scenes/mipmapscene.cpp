#include "mipmapscene.h"

#include <iostream>
#include <algorithm>
#include "../extlibs/imgui/imgui.h"
#include "../extlibs/imgui/imguiRenderGL3.h"

MipMapScene::MipMapScene()
{
}

MipMapScene::~MipMapScene()
{
	imguiRenderGLDestroy();
}

void MipMapScene::init()
{
	std::string fontfolder = settings->getResourcesFolder() + "fonts/DroidSans.ttf";
	if (!imguiRenderGLInit(fontfolder.c_str()))
	{
		fprintf(stderr, "Could not init GUI renderer.\n");
		exit(EXIT_FAILURE);
	}

	auto sz0 = settings->getResourcesFolder() + "textures/stpeter_m00_c00.png";
	auto sz1 = settings->getResourcesFolder() + "textures/stpeter_m00_c01.png";
	auto sz2 = settings->getResourcesFolder() + "textures/stpeter_m00_c02.png";
	auto sz3 = settings->getResourcesFolder() + "textures/stpeter_m00_c03.png";
	auto sz4 = settings->getResourcesFolder() + "textures/stpeter_m00_c04.png";
	auto sz5 = settings->getResourcesFolder() + "textures/stpeter_m00_c05.png";

	diffuse.createFromFilesCube(sz0.c_str(), sz1.c_str(), sz2.c_str(), sz3.c_str(), sz4.c_str(), sz5.c_str());
	diffuse.generateMipChain();
	lvls = 0.0f;

	auto str1 = settings->getResourcesFolder() + "shaders/basicmip.vert";
	auto str2 = settings->getResourcesFolder() + "shaders/basicmip.frag";

	mipmapv.compileShader(str1.c_str(), ShaderType::VERTEX);
	mipmapf.compileShader(str2.c_str(), ShaderType::FRAGMENT);
	mipmapprog.create(mipmapv.getHandle(), mipmapf.getHandle());


	quad.loadFromFile(settings->getResourcesFolder() + "models/head.obj");

	cam.setPosition(vec3f(0,0,4));
	cam.setViewportAspectRatio(settings->getAspect());

	elapsedTime = 0;

	diffuse.bind(0);

}

void MipMapScene::handleEvents(const SDL_Event& event)
{
	//If user moves the mouse
	if (event.type == SDL_MOUSEMOTION)
	{
		int x = event.motion.x - settings->getWindowWidth()/2;
		int y = event.motion.y - settings->getWindowHeight()/2;
		cam.offsetOrientation(0.05f * (float)y, 0.05f * (float)x);
	}

}

void MipMapScene::updateFixed(unsigned long long dt)
{
	SDL_PumpEvents();
	int statelen;
	const Uint8 *state = SDL_GetKeyboardState(&statelen);

	//If user presses any key
	if (state[SDL_SCANCODE_S]) cam.offsetPosition(0.05f * -cam.forward());
	if (state[SDL_SCANCODE_W]) cam.offsetPosition(0.05f * cam.forward());
	if (state[SDL_SCANCODE_A]) cam.offsetPosition(0.05f * -cam.right());
	if (state[SDL_SCANCODE_D]) cam.offsetPosition(0.05f * cam.right());
	if (state[SDL_SCANCODE_Z]) cam.offsetPosition(0.05f * -vec3f(0, 1, 0));
	if (state[SDL_SCANCODE_X]) cam.offsetPosition(0.05f * vec3f(0, 1, 0));
	if(state[SDL_SCANCODE_DOWN]) lvls += 0.1f;
	if(state[SDL_SCANCODE_UP]) lvls -= 0.1f;

	lvls = std::max(0.0f, std::min(lvls, static_cast<float>(diffuse.getMipmapLevels())));

	elapsedTime += static_cast<float>(dt) / 5000000.f;
}

void MipMapScene::update(unsigned long long dt)
{

}

void MipMapScene::draw(unsigned long long dt)
{
	mipmapprog.bind();
	mipmapprog.setUniform("View", cam.view());
	mipmapprog.setUniform("Proj", cam.projection());
	mipmapprog.setUniform("tex", diffuse.getUnit());
	mipmapprog.setUniform("lvls", lvls);
	quad.bind();
	quad.draw();
	quad.unbind();
	mipmapprog.unbind();
	//drawGui();
}

void MipMapScene::drawGui()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);

	imguiBeginFrame(0, 0, 0, 0);
	imguiBeginScrollArea("Scroll area", 10, 10, settings->getWindowWidth() / 5, settings->getWindowHeight() - 20, &states.scrollarea1);
	imguiSeparatorLine();
	imguiSeparator();
	imguiButton("Button");
	imguiButton("Disabled button", false);
	imguiItem("Item");
	imguiItem("Disabled item", false);
	imguiEndScrollArea();
	imguiEndFrame();
	imguiRenderGLDraw(settings->getWindowWidth(), settings->getWindowHeight());
}

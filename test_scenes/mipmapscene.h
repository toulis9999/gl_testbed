#ifndef MIPMAPSCENE_H
#define MIPMAPSCENE_H
#include "../include/scene.h"

#include "../include/mesh.h"
#include "../include/camera.h"
#include "../include/texture.h"
#include "../include/shader.h"
#include "../include/program.h"
#include "../include/framebuffer.h"


class MipMapScene: public Scene
{
public:
	MipMapScene();
	virtual ~MipMapScene();

	virtual void init() override;
	virtual void handleEvents(const SDL_Event& event) override;
	virtual void updateFixed(unsigned long long dt) override;
	virtual void update(unsigned long long dt) override;
	virtual void draw(unsigned long long dt) override;

private:
	struct GuiStates
	{
		bool checked1 = false;
		bool checked2 = false;
		bool checked3 = true;
		bool checked4 = false;
		float value1 = 50.f;
		float value2 = 30.f;
		int scrollarea1 = 0;
		int scrollarea2 = 0;
	};

	void drawGui();

	GuiStates states;

	Mesh quad;
	Camera cam;

	float lvls;

	Texture diffuse;
	Shader mipmapv, mipmapf;
	Program mipmapprog;
	float elapsedTime;
};

#endif // MIPMAPSCENE_H
